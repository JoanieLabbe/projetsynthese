# Rétrospective ALPHA 1
## Points positifs
* Nous faisons souvent des réunions quotidiennes, alors nous sommes toujours au courant de ce sur quoi les autres travaillent.
* Lorsqu'elle est demandée, de l'aide est apportée rapidement.
* Les commentaires des révisions de code sont pertinent.
* Les communications sont cordiales et civilisées.
* Personne n'a brisé le dépot GIT.
## Points à améliorer
| Élément | Piste de solution |
| :------ | :---------------- |
| Parfois, on coupe ou on enterre les autres pendant les rencontres | On pourrait ouvrir les caméras et lever physiquement la main, ainsi il y a plus de chance qu'on le vois |
| Mettre à jour les récits d'utilisateurs de manière plus assidue | Aller regarder le tableau fréquement, soit au moins deux à trois fois par cours |
| Ne pas oublier de remplir les évaluations des pairs chaque semaine | S'ajouter un rappel automatique dans le calendrier pour ne pas oublier de le faire |