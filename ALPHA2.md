# Rétrospective ALPHA 2
## Points positifs
* Lorsque l'on a une demande de fusionner notre code, nous avons des retours rapidement.
* La coopération est bonne.
* Il est facile de recevoir de l'aide et cette aide est respectueuse et efficace.
* Les scrums sont efficaces, complets et professionnels. Il est plus facile de voir l'évolution du projet.
* L'équipe est compréhensive.
* Lorsque l'on lève la main, on accède rapidement au droit de parole.
* Le projet n'est jamais sur pause, parce que l'on continue de s'alimenter en tâches.

## Points à améliorer
| Élément | Piste de solution |
| :------ | :---------------- |
| S'il y a un problème en parler directement à la personne concernée, plutôt que de le laisser s'empirer | S'adresser à la personne concernée et rester réceptif |
| Parfois certaines idées sont oubliées ou pas prise en considération | Avoir un canal pour toutes les gardées en notes et continuer de poussé son idée dans les limites du raisonnables |
| Les tâches semblent parfois réparties inégalement ce qui inquiète certaines personne sur s'ils pourront encore des tâches pour le reste du sprint | La mise en place de mandat pré-assigné à quelqu'un dont les tâches ne sont fait que par cette personne |
| Les scrums sont trop long et on a tendance à parler des bugs pendant une éternité au milieu de la rencontre | La mise en place d'une période de résolution à la fin du scrum ou ceux qui sont concernés travail à la résolution |