﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.GameController)]
    public class AchievementPublisher : MonoBehaviour
    {
        private AchievementEventChannel achievementEventChannel;
        
        private void Awake()
        {
            achievementEventChannel = Finder.AchievementEventChannel;
        }

        public void UnlockAchievement(AchievementType achievementType)
        {
            achievementEventChannel.Publish(achievementType, DateTime.Now);
        }

        public void UnlockAllCardsAchievement(Level3CardsAchievement cardsAchievement, string itemTag)
        {
            if (cardsAchievement.VerifyEligibility(itemTag))
            {
                UnlockAchievement(AchievementType.Level3CollectedAllCards);
            }
        }

        public void UnlockOnTimeAchievement(HUDTimerController hudTimerController)
        {
            if (hudTimerController.VerifyEligibility())
            {
                UnlockAchievement(AchievementType.Level3CompletedOnTime);
            }
        }
    }
}