﻿namespace Game
{
    // Author : Carol-Ann Collin
    public enum AchievementType
    {
        LevelCompleted,
        PlayerHitByVehicle,
        Level1PutAllDogToysInBox,
        Level1TeethBrushed,
        Level1DogFed,
        Level2BallReturnedToPark,
        Level3CompletedOnTime,
        Level3CollectedAllCards,
        Level1And3TookAllCoffee,
        Level5HitByChildOnSwing,
        Level5KeysPickedUp,
        CoffeeTaken
    }
}