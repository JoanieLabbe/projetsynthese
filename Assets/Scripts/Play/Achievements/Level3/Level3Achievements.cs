﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class Level3Achievements : MonoBehaviour
    {
        private HUDTimerController hudTimerController;
        private Level3CardsAchievement cardsAchievement;
        private AchievementPublisher achievementPublisher;
        private InventoryEventChannel inventoryEventChannel;
        private LevelCompleteEventChannel levelCompleteEventChannel;
        
        private void Awake()
        {
            hudTimerController = Finder.HUDTimerController;
            cardsAchievement = FindObjectOfType<Level3CardsAchievement>();
            
            achievementPublisher = Finder.AchievementPublisher;
            
            inventoryEventChannel = Finder.InventoryEventChannel;
            inventoryEventChannel.OnItemAcquired += OnItemAcquired;
                    
            levelCompleteEventChannel = Finder.LevelCompleteEventChannel;
            levelCompleteEventChannel.OnLevelComplete += OnLevelComplete;
        }
        
        private void OnDestroy()
        {
            inventoryEventChannel.OnItemAcquired -= OnItemAcquired;
            levelCompleteEventChannel.OnLevelComplete -= OnLevelComplete;
        }

        private void OnLevelComplete()
        {
            achievementPublisher.UnlockOnTimeAchievement(hudTimerController); 
        }

        private void OnItemAcquired(InventoryItem inventoryItem)
        {
            achievementPublisher.UnlockAllCardsAchievement(cardsAchievement, inventoryItem.Tag);
        }
    }
}