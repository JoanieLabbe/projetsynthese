﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class Level3CardsAchievement : MonoBehaviour
    {
        private int cardCount;
        private const string CARD = "Card";
        private Inventory inventory;

        private void Awake()
        {
            inventory = Finder.Inventory;
            cardCount = transform.childCount;
        }

        public bool VerifyEligibility(string itemTag)
        {
            return itemTag == CARD && inventory.GetItemByTag(CARD).Quantity == cardCount;
        }
    }
}