﻿using System;
using System.Globalization;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.GameController)]
    public class AchievementFileWriter : MonoBehaviour
    {
        private AchievementEventChannel achievementEventChannel;
        private SaveFileSelection saveFile;

        private void Awake()
        {
            achievementEventChannel = Finder.AchievementEventChannel;
            saveFile = Finder.SaveFileSelection;
        }
        
        public void CreateAchievementEntry(int level, AchievementType achievementType, DateTime unlockedTime)
        {
            AchievementEntry entry = new AchievementEntry(level, achievementType, unlockedTime.ToString(CultureInfo.CurrentCulture));
            if(achievementType != AchievementType.CoffeeTaken && !IsAchievementUnlocked(entry))
            {
                saveFile.AddAchievementIntoList(entry);
                saveFile.SaveFileValue();
            }
            else
            {
                ManageCoffee(level);
            }
        }

        private void ManageCoffee(int level)
        {
            switch (level)
            {
                case 1:
                    saveFile.SetCoffeeAsTaken();
                    saveFile.SaveFileValue();
                    break;
                    
                case 3 when saveFile.LoadFileValue().level1CoffeeTaken:
                    achievementEventChannel.Publish(AchievementType.Level1And3TookAllCoffee, DateTime.Now);
                    break;
            }
        }

        private bool IsAchievementUnlocked(AchievementEntry entry)
        {
            return Enumerable.Contains(saveFile.LoadFileValue().unlockedAchievements, entry);
        }
    }
}