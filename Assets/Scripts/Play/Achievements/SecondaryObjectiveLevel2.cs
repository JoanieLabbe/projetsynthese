﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class SecondaryObjectiveLevel2 : Recyclable
    {
        private AchievementPublisher achievementPublisher;
        
        private void Awake()
        {
            achievementPublisher = Finder.AchievementPublisher;
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.gameObject.CompareTag(Tags.Objective))
            {
                achievementPublisher.UnlockAchievement(AchievementType.Level2BallReturnedToPark);
                Recycle();
            }
        }
    }
}