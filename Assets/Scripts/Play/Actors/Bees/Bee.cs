﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    public class Bee : MonoBehaviour
    {
        [SerializeField] private PolygonCollider2D flyingZone;
        
        private FlagUpdates flag;
        private BeeMover beeMover;
        private DestinationSelecter selecter;
        
        private void Awake()
        {
            flag = Finder.FlagUpdates;
            beeMover = GetComponent<BeeMover>();
            selecter = GetComponent<DestinationSelecter>();
            
            selecter.DetermineMovingLimits(transform.position, flyingZone);
            
            beeMover.CurrentDestination = selecter.FindDestination();
        }

        private void Update()
        {
            if (flag.EnemyShouldUpdate)
            {
                beeMover.Move();

                if (beeMover.IsAtDestination)
                {
                    beeMover.CurrentDestination = selecter.FindDestination();
                }
            }
        }
    }
}