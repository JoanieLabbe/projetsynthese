﻿namespace Game
{
    // Author : Kamylle Thériault
    public class DialoguePerson : MovingCharacters
    {
        protected override void Update()
        {
            if (flag.DialoguePersonShouldUpdate)
            {
                UpdateCharacter();
            }
        }
    }
}