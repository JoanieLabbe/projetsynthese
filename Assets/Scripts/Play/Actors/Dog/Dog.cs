﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class Dog : MonoBehaviour
    {
        private Inputs playerInputs;
        private DogWithPlayerMover dogMover;
        private LightingSource2D lightSource;
        
        private void Awake()
        {
            playerInputs = Finder.Inputs;
            dogMover = GetComponent<DogWithPlayerMover>();
            lightSource = GetComponentInParent<LightingSource2D>();
        }

        private void Update()
        {
            dogMover.Move(playerInputs.Actions.Game.Move.ReadValue<Vector2>());
        }

        // Author = Marc-Antoine Sigouin
        public void ToggleLightSource (bool isEnabled)
        {
            lightSource.enabled = isEnabled;
        }
    }
}