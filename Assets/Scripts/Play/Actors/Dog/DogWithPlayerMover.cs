﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class DogWithPlayerMover : MonoBehaviour
    {
        private SpriteRenderer renderer;
        private SpriteChanger changer;

        private void Awake()
        {
            renderer = GetComponent<SpriteRenderer>();
            changer = GetComponent<SpriteChanger>();
        }

        public void Move(Vector2 movement)
        {
            if (movement != Vector2.zero)
            {
                transform.parent.RotateAround(transform.parent.parent.position, transform.parent.up, Vector2.Angle(Vector2.zero, movement));
            }
            Rotate(movement);
        }
        
        // Author : Vivianne Lord
        private void Rotate(Vector2 direction)
        {
            renderer.transform.up = Vector2.zero;
            changer.DetermineSpriteToUse(direction);
        }
    }
}