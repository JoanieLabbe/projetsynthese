﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class InteractibleDog : Recyclable, IInteractable
    {
        [SerializeField] private GameObject playerVariant;
        [SerializeField] private InventoryItem harness;
        
        private FlagUpdates flag;
        private PathMover dogMover;
        private Main main;
        private Inventory inventory;
        private PlayerChangeEventChannel playerChangeEventChannel;
        private ActiveCameraChanger activeCameraChanger;
        private ObjectivesEventChannel objectivesEventChannel;
        private CutsceneEventChannel cutsceneEventChannel;

        // Author : Marc-Antoine Sigouin
        private void Awake()
        {
            main = Finder.Main;
            flag = Finder.FlagUpdates;
            dogMover = GetComponent<PathMover>();
            inventory = Finder.Inventory;
        }

        // Appeler dans le Start pour éviter des NullException
        private void Start()
        {
            playerChangeEventChannel = Finder.PlayerChangeEventChannel;
            activeCameraChanger = Finder.ActiveCameraChanger;
            objectivesEventChannel = Finder.ObjectivesEventChannel;
            cutsceneEventChannel = Finder.CutsceneEventChannel;
        }

        // Author : Marc-Antoine Sigouin
        private void Update()
        {
            if (dogMover != null && flag.InteractibleDogShouldUpdate)
            {
                dogMover.Move();
            }
        }

        public void StartInteraction(Player sender)
        {
            if (harness != null)
            {
                if (inventory.GetItemByTag(harness.Tag) == null)
                {
                    return;
                }
            }
            
            PlayerTakesDog(sender);
        }

        // Author : Vivianne Lord
        private void PlayerTakesDog(Player sender)
        {
            sender.RemoveOldPlayer();
            playerVariant.transform.position = sender.transform.position;
            playerVariant.SetActive(true);
            playerChangeEventChannel.Publish(playerVariant.GetComponent<Player>());
            Recycle();
            activeCameraChanger.DeactivateCurrentCamera(playerVariant.transform);
            objectivesEventChannel.PublishCompleted();

            if (main.LevelToLoad == 5)
            {
                cutsceneEventChannel.PublishRegularTrigger();
            }
        }

        public void EndInteraction()
        {
            // Rien à faire
        }
    }
}