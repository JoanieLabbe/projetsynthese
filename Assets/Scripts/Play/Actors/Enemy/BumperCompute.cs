﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class BumperCompute : MonoBehaviour
    {
        [Range(0, 1)]
        [SerializeField] private float bumpForcePercent = 0.4f;
        private PathMover pathMover;

        private Vector3 direction => Vector3.Normalize(pathMover.Waypoint.transform.position - transform.position);
        
        private void Awake()
        {
            pathMover = GetComponent<PathMover>();
        }

        public void Bump(PlayerMover playerMover)
        {
            playerMover.Bump(CalculateForce(playerMover) * bumpForcePercent, pathMover.Speed);
        }

        private Vector3 CalculateForce(PlayerMover playerMover)
        {
            return direction + GetDistanceFromPlayer(playerMover);
        }

        private Vector3 GetDistanceFromPlayer(PlayerMover playerMover)
        {
            return playerMover.transform.position - transform.position;
        }
    }
}