﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class Child : MonoBehaviour
    {
        [SerializeField] private Swings swing;
        [SerializeField] private GameObject objectToSpawn;
        
        public event ChildHasDespawned OnChildDespawn;

        private AchievementPublisher publisher;
        private PathMover pathMover;
        private bool hasBumpedIntoPlayer;

        private void Awake()
        {
            publisher = Finder.AchievementPublisher;
            pathMover = GetComponent<PathMover>();
            pathMover.OnPathFinish += OnPathEnd;
            swing.Child = this;
            hasBumpedIntoPlayer = false;
        }

        private void OnDestroy()
        {
            pathMover.OnPathFinish -= OnPathEnd;
        }

        private void OnPathEnd()
        {
            if (OnChildDespawn != null)
            {
                OnChildDespawn();
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var otherGameObject = other.gameObject;
            var playerMover = otherGameObject.GetComponent<PlayerMover>();
            if (playerMover != null && !hasBumpedIntoPlayer)
            {
                hasBumpedIntoPlayer = true;
                objectToSpawn.transform.position = playerMover.transform.position;
                objectToSpawn.SetActive(true);
                
                publisher.UnlockAchievement(AchievementType.Level5HitByChildOnSwing);
            }
        }
    }

    public delegate void ChildHasDespawned();
}