﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class CollisionStimulus : MonoBehaviour
    {
        private BrakeAssist brakeAssist;

        private void Awake()
        {
            brakeAssist = GetComponent<BrakeAssist>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var otherGameObject = other.gameObject;
            if (otherGameObject.CompareTag(transform.parent.gameObject.tag))
            {
                brakeAssist.Break = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var otherGameObject = other.gameObject;
            if (otherGameObject.CompareTag(transform.parent.gameObject.tag))
            {
                brakeAssist.Break = false;
            }
        }
    }
}