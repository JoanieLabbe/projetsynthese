﻿namespace Game
{
    // Author : Carol-Ann Collin
    public interface IRoadUser
    {
        void ReceiveRoadSignal(bool shouldStop);
    }
}