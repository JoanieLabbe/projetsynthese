﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin, Joanie Labbe
    public class Pedestrian : MonoBehaviour, IRoadUser
    {
        [SerializeField] private float delay = 15f;

        private PedestrianCrossingEventChannel pedestrianCrossingEventChannel;
        private PathMover pathMover;
        
        private void Awake()
        {
            pedestrianCrossingEventChannel = Finder.PedestrianCrossingEventChannel;
            pathMover = GetComponent<PathMover>();
        }

        public void ReceiveRoadSignal(bool shouldStop)
        {
            pathMover.Pause(!shouldStop);
            
            if (pathMover.IsPaused)
            {
                pedestrianCrossingEventChannel.OnStateChange += Restart;
                Waiting();
            }
        }

        private void Restart(bool isActive)
        {
            pedestrianCrossingEventChannel.OnStateChange -= Restart;
            ReceiveRoadSignal(isActive);
        }

        // Author : Joanie Labbe
        private void Waiting()
        {
            IEnumerator WaitingRoutine()
            {
                yield return new WaitForSeconds(delay);
                if (pathMover.IsPaused)
                {
                    pedestrianCrossingEventChannel.PublishWaitLimitExceeded();
                }
            }

            StartCoroutine(WaitingRoutine());
        }
    }
}