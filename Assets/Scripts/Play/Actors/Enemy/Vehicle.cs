﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin, Joanie Labbe
    public class Vehicle : MonoBehaviour, IRoadUser
    {
        private PedestrianCrossingEventChannel pedestrianCrossingEventChannel;
        private PathMover pathMover;
        
        private void Awake()
        {
            pedestrianCrossingEventChannel = Finder.PedestrianCrossingEventChannel;
            pathMover = GetComponent<PathMover>();
        }

        public void ReceiveRoadSignal(bool shouldStop)
        {
            pathMover.Pause(shouldStop);
            
            if (pathMover.IsPaused)
            {
                pedestrianCrossingEventChannel.OnStateChange += Restart;
            }
        }
        
        private void Restart(bool isActive)
        {
            pedestrianCrossingEventChannel.OnStateChange -= Restart;
            ReceiveRoadSignal(isActive);
        }
    }
}