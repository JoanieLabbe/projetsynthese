﻿namespace Game
{
    // Author : Joanie Labbé
    /// <summary>
    /// Doit être implementé par tous les objets qui peuvent être "vu" par le sonar
    /// </summary>
    public interface IHearable
    {
        /// <summary>
        /// Cause une rétroaction visuelle pour indiquer qu'on "l'entend"
        /// </summary>
        void Hear();
    }
}