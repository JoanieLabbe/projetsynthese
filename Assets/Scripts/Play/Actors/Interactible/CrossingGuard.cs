﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class CrossingGuard : MonoBehaviour, IInteractable
    {
        private Soundable soundable;
        private PedestrianCallButton pedestrianCallButton;
        private PathMover pathMover;
        private NoisyObject noisyObject;

        private bool shouldMove;
        
        private void Awake()
        {
            soundable = GetComponentInChildren<Soundable>();
            pedestrianCallButton = GetComponentInChildren<PedestrianCallButton>();
            pathMover = GetComponent<PathMover>();
            pathMover.OnPathFinish += OnPathFinish;
            shouldMove = false;
        }

        private void Start()
        {
            noisyObject = GetComponentInChildren<NoisyObject>();
        }

        public void StartInteraction(Player sender)
        {
            shouldMove = true;
            soundable.StartInteraction(sender);
            pedestrianCallButton.StartInteraction(sender);
        }

        public void EndInteraction()
        {
            soundable.EndInteraction();
            pedestrianCallButton.EndInteraction();
        }

        private void Update()
        {
            if (shouldMove)
            {
                pathMover.Move();
                noisyObject.MakeSound();
            }
            else
            {
                noisyObject.StopMakeSound();
            }
        }

        private void OnDestroy()
        {
            pathMover.OnPathFinish -= OnPathFinish;
        }

        private void OnPathFinish()
        {
            shouldMove = false;
        }
    }
}