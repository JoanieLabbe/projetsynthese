﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class DoorOpener : MonoBehaviour, IInteractable
    {
        public void StartInteraction(Player sender)
        {
            Finder.DoorAccessRequestedEventChannel.Publish(gameObject);
        }

        public void EndInteraction()
        {
           // Rien à faire 
        }
    }
}