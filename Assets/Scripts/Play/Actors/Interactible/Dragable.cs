﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé, Vivianne Lord
    public class Dragable : MonoBehaviour, IInteractable
    {
        private SilentObject silentObject;
        private CircleCollider2D circleCollider2D;
        public bool IsInteractedWith { get; private set; }

        private void Awake()
        {
            silentObject = GetComponent<SilentObject>();
            circleCollider2D = GetComponent<CircleCollider2D>();
        }

        public void StartInteraction(Player sender)
        {
            if (transform.parent == null)
            {
                IsInteractedWith = true;
                transform.parent = sender.transform;
                circleCollider2D.isTrigger = true;
                silentObject.ShouldBeActive = false;
                silentObject.MakeLightVisible();
            }
        }

        public void EndInteraction()
        {
            if (transform.parent != null)
            {
                IsInteractedWith = false;
                transform.parent = null;
                circleCollider2D.isTrigger = false;
                silentObject.ShouldBeActive = true;
                silentObject.MakeLightInvisible();
            }
        }
    }
}