﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé, Vivianne Lord
    public class InteractableSensor : MonoBehaviour
    {
        public List<IInteractable> InteractableObjects { get; set; }
        private List<IInteractable> objectsToDelete;

        private void Awake()
        {
            InteractableObjects = new List<IInteractable>();
            objectsToDelete = new List<IInteractable>();
        }

        private void Update()
        {
            if (objectsToDelete.Count > 0)
            {
                objectsToDelete.ForEach(obj => InteractableObjects.Remove(obj));
                objectsToDelete.Clear();
            }
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            IInteractable interactableObject = other.gameObject.GetComponent<IInteractable>();
            if (interactableObject != null && !InteractableObjects.Contains(interactableObject))
            {
                InteractableObjects.Add(interactableObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            IInteractable interactableObject = other.gameObject.GetComponent<IInteractable>();
            if (interactableObject != null && !objectsToDelete.Contains(interactableObject))
            {
                objectsToDelete.Add(interactableObject);
            }
        }
    }
}