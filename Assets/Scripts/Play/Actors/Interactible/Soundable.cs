﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class Soundable : MonoBehaviour, IInteractable
    {
        private NoisyObject noisyObject;
        private SilentObject silentObject;
        
        protected void Awake()
        {
            noisyObject = gameObject.AddComponent<NoisyObject>();
            silentObject = gameObject.AddComponent<SilentObject>();
        }

        private void Start()
        {
            noisyObject.StopMakeSound();
            noisyObject.enabled = false;
        }

        private void Update()
        {
            if (!silentObject.ShouldBeActive && !noisyObject.IsSoundPlaying)
            {
                noisyObject.StopMakeSound();
                noisyObject.enabled = false;
                silentObject.ShouldBeActive = true;
            }
        }

        public void StartInteraction(Player sender)
        {
            noisyObject.enabled = true;
            noisyObject.MakeSound();
            silentObject.ShouldBeActive = false;
        }

        public void EndInteraction()
        {
            // Rien à faire
        }
    }
}