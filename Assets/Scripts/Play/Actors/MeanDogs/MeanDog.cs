﻿using System;
using Harmony;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class MeanDog : MonoBehaviour
    {
        [Header("Dog settings")]
        [SerializeField] private PolygonCollider2D dogTerritory;
        [SerializeField] private AudioClip growling;
        [SerializeField] private AudioClip barking;

        private const float DOG_UNEASY_RADIUS = 4f;
        private const float DOG_ATTACKING_RADIUS = 3f;
        private const float MIN_DOG_WAIT_TIME = 2f;
        private const float MAX_DOG_WAIT_TIME = 5f;

        private CurrentPlayer currentPlayer;
        private FlagUpdates flag;
        private MeanDogMover meanMover;
        private DestinationSelecter selecter;
        private SoundPlayer soundPlayer;
        

        private float waitDelay;

        private bool isDoneWaiting => waitDelay > 0f;

        private MeanDogMood mood;
        private MeanDogMood oldMood;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            meanMover = GetComponent<MeanDogMover>();
            currentPlayer = Finder.CurrentPlayer;
            selecter = GetComponent<DestinationSelecter>();
            soundPlayer = Finder.SoundPlayer;
            
            selecter.DetermineMovingLimits(transform.position, dogTerritory);
            
            meanMover.CurrentDestination = selecter.FindDestination();
        }

        private void Update()
        {
            if (flag.EnemyShouldUpdate && meanMover != null)
            {
                DetermineDogMood();
                
                PlayMoodSound();

                switch (mood)
                {
                    case MeanDogMood.HappyDog:
                        if (!isDoneWaiting)
                        {
                            meanMover.Move();
                        }
                        else
                        {
                            Wait();
                        }

                        if (meanMover.IsAtDestination && !isDoneWaiting)
                        {
                            TakeABreakOrWalk();
                        }
                        break;
                    
                    case MeanDogMood.UnsureDog:
                        meanMover.WaitWhileLookingAtObject(currentPlayer.Player);
                        break;
                        
                    case MeanDogMood.AngryDog:
                        meanMover.RunTowardsObject(currentPlayer.Player);
                        break;
                }
            }
        }

        private void DetermineDogMood()
        {
            float distance = Vector3.Distance(transform.position, currentPlayer.Player.transform.position);

            oldMood = mood;

            if (distance <= DOG_ATTACKING_RADIUS && dogTerritory.OverlapPoint(currentPlayer.Player.transform.position))
            {
                mood = MeanDogMood.AngryDog;
            }
            else if (distance <= DOG_UNEASY_RADIUS || dogTerritory.OverlapPoint(currentPlayer.Player.transform.position))
            {
                mood = MeanDogMood.UnsureDog;
            }
            else
            {
                mood = MeanDogMood.HappyDog;
            }
        }

        private void TakeABreakOrWalk()
        {
            if (Random.Range(0, 10) < 2)
            {
                meanMover.WaitCalmly();
                waitDelay = Random.Range(MIN_DOG_WAIT_TIME, MAX_DOG_WAIT_TIME);
            }
            else
            {
                meanMover.CurrentDestination = selecter.FindDestination();
            }
        }

        private void Wait()
        {
            waitDelay -= Time.deltaTime;
            
            if (!isDoneWaiting)
            {
                meanMover.CurrentDestination = selecter.FindDestination();
            }
        }

        private void PlayMoodSound()
        {
            if (mood != oldMood)
            {
                switch (oldMood)
                {
                    case MeanDogMood.UnsureDog:
                        soundPlayer.StopSound(growling);
                        break;
                    case MeanDogMood.AngryDog:
                        soundPlayer.StopSound(barking);
                        break;
                }

                switch (mood)
                {
                    case MeanDogMood.UnsureDog:
                        soundPlayer.PlaySound(growling, 0.25f, true);
                        break;
                    case MeanDogMood.AngryDog:
                        soundPlayer.PlaySound(barking);
                        break;
                }
            }
        }
        
        // Author : Joanie Labbé
        private void OnCollisionEnter2D(Collision2D other)
        {
            var otherGameObject = other.gameObject;
            IHurtable objectToHurt = otherGameObject.GetComponent<IHurtable>();
            if (objectToHurt != null)
            {
                objectToHurt.Hurt();
            }
        }
    }
}