﻿namespace Game
{
    // Author : Marc-Antoine Sigouin
    public enum MeanDogMood
    {
        HappyDog,
        UnsureDog,
        AngryDog
    }
}