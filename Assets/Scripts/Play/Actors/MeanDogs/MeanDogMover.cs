﻿using System;
using UnityEngine;

namespace Game
{
    public class MeanDogMover : MonoBehaviour
    {
        [SerializeField] private float normalSpeed = 1f;

        private const float TOLERANCE = 0.05f;
        private SpriteChanger spriteChanger;
        private Vector3 currentPosition => transform.position;
        private float runningSpeed => normalSpeed * 3;
        
        public Vector3 CurrentDestination { get; set; }

        public bool IsAtDestination => Vector2.Distance(transform.position, CurrentDestination) <= TOLERANCE;

        private void Awake()
        {
            spriteChanger = GetComponent<SpriteChanger>();
        }

        public void Move()
        {
            Vector3 movement = CurrentDestination - currentPosition;
            movement.Normalize();
            
            spriteChanger.DetermineSpriteToUse(movement);
            
            movement *= normalSpeed * Time.deltaTime;
            transform.Translate(movement);
        }

        public void WaitCalmly()
        {
            spriteChanger.DetermineSpriteToUse(Vector3.zero);
        }

        public void WaitWhileLookingAtObject(Player playerToLookAt)
        {
            Vector3 movement = playerToLookAt.transform.position - currentPosition;
            movement.Normalize();
            spriteChanger.DetermineSpriteToUse(movement);
        }

        public void RunTowardsObject(Player target)
        {
            Vector3 movement = target.transform.position - currentPosition;
            movement.Normalize();
            
            spriteChanger.DetermineSpriteToUse(movement);
            
            movement *= runningSpeed * Time.deltaTime;
            transform.Translate(movement);
        }
    }
}