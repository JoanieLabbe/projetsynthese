﻿using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class NoisyObject : MonoBehaviour
    {
        [SerializeField] private float delay = 5f;
        
        [Header("Light properties")]
        [SerializeField][Range(1, 10)] protected float maxLightSize = 2f;
        [SerializeField][Min(0)] protected float minLightSize;

        private LightingSource2D lightSource;
        private AtmosphereSound atmosphereSound;
        private bool shouldChangeState = true;

        public bool IsSoundPlaying => atmosphereSound.IsPlaying;

        private void Awake()
        {
            lightSource = gameObject.GetComponentInChildren<LightingSource2D>();
            lightSource.lightSize = minLightSize;
            atmosphereSound = GetComponent<AtmosphereSound>();
        }

        private void Update()
        {
            if (atmosphereSound.IsPlaying)
            {
                lightSource.DotweenTo(maxLightSize, delay);
                StartCoroutine(shouldChangeState.ChangeState(delay));
            }
            else if (!atmosphereSound.IsPlaying && shouldChangeState)
            {
                lightSource.DotweenTo(minLightSize, delay);
                StartCoroutine(shouldChangeState.ChangeState(delay));
            }
        }

        public void MakeSound()
        {
            if (!atmosphereSound.IsPlaying)
            {
                atmosphereSound.StartSound();
            }
        }

        public void StopMakeSound()
        {
            atmosphereSound.StopSound();
            lightSource.DotweenTo(minLightSize, delay);
        }
    }
}