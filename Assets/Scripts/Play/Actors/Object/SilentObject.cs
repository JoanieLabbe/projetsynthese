﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé, Vivianne Lord
    public class SilentObject : MonoBehaviour, IHearable
    {
        [Header("Light properties")]
        [SerializeField][Range(1, 10)] protected float maxLightSize = 2f;
        [SerializeField][Min(0)] protected float minLightSize;
        [SerializeField] private float delay = 5f;

        private GameObject lightObject;
        private bool isHeard;
        private LightingSource2D lightSource;

        public bool ShouldBeActive { get; set; }

        private void Awake()
        {
            lightSource = gameObject.GetComponentInChildren<LightingSource2D>();
            lightSource.lightSize = minLightSize;
            ShouldBeActive = true;
        }

        public void Hear()
        {
            if (!ShouldBeActive)
            {
                return;
            }
            
            IEnumerator IsHeardCoroutine()
            {
                MakeLightVisible();
                yield return new WaitForSeconds(delay);
                MakeLightInvisible();
            }
            
            if (!isHeard)
            {
                StartCoroutine(IsHeardCoroutine());
            }
        }

        public void MakeLightInvisible()
        {
            if (ShouldBeActive)
            {
                lightSource.DotweenTo(minLightSize, delay);
                isHeard = false;
            }
        }

        public void MakeLightVisible()
        {
            isHeard = true;
            lightSource.DotweenTo(maxLightSize, delay);
        }
    }
}