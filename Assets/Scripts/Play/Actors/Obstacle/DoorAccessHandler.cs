﻿using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class DoorAccessHandler : MonoBehaviour
    {
        [SerializeField] private List<InventoryItem> doorAccessRequiredItems;
        [SerializeField] [Range(0, 10)] private int requiredItemQuantity;
        [SerializeField] private AudioClip grantedAccess;
        [SerializeField] private AudioClip deniedAccess;

        private SoundPlayer soundPlayer;
        private Inventory inventory;
        private DoorAccessRequestedEventChannel doorAccessRequestedEventChannel;

        private void Awake()
        {
            inventory = Finder.Inventory;
            soundPlayer = Finder.SoundPlayer;
            
            doorAccessRequestedEventChannel = Finder.DoorAccessRequestedEventChannel;
            doorAccessRequestedEventChannel.OnDoorAccessRequested += OnDoorAccessRequested;
        }

        private void OnDestroy()
        {
            doorAccessRequestedEventChannel.OnDoorAccessRequested -= OnDoorAccessRequested;
        }

        private void OnDoorAccessRequested(GameObject door)
        {
            if (gameObject == door)
            {
                List<Item> currentItems = new List<Item>();
                doorAccessRequiredItems.ForEach(item => currentItems.Add(inventory.GetItemByTag(item.Tag)));

                if (currentItems.Any(item => item == null || item.Quantity != requiredItemQuantity))
                {
                    PlaySound(deniedAccess);
                    return;
                }

                PlaySound(grantedAccess);
                door.SetActive(false);
            }
        }

        private void PlaySound(AudioClip clip)
        {
            if (clip != null)
            {
                soundPlayer.PlaySound(clip);
            }
        }
    }
}