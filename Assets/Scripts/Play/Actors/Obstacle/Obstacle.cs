﻿using System.Collections;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class Obstacle : MonoBehaviour, IHearable
    {
        [Header("Obstacle")]
        [SerializeField] private bool isColliderCircle = false;
        [SerializeField][Min(0.5f)] private float obstacleLength = 4f;
        [SerializeField][Min(0.5f)] private float obstacleWidth = 2f;
        [SerializeField][Min(1)] private float delay = 5f;
        [SerializeField][Range(0, 1)] private float percentageLight = 0.5f;

        [Header("Sprites")]
        [SerializeField] private Sprite lightSprite;

        private bool isHeard;
        private Vector2 size;
        private BoxCollider2D boxCollider;
        private CircleCollider2D circleCollider;
        private LightingSpriteRenderer2D lightSpriteRenderer;

        private void Awake()
        {
            size = new Vector2(obstacleLength, obstacleWidth);
            
            ManageComponents();
            SetSprites();
            AdjustSize();
        }

        private void ManageComponents()
        {
            lightSpriteRenderer = gameObject.GetComponentInChildren<LightingSpriteRenderer2D>();
            
            if (!isColliderCircle)
            {
                boxCollider = gameObject.AddComponent<BoxCollider2D>();
            }
            else
            {
                circleCollider = gameObject.AddComponent<CircleCollider2D>();
            }
        }
        
        private void SetSprites()
        {
            lightSpriteRenderer.sprite = lightSprite;
        }
        
        private void AdjustSize()
        {
            if (!isColliderCircle)
            {
                boxCollider.size = size;
            }
            else
            {
                circleCollider.radius = obstacleLength / 2;
            }

            lightSpriteRenderer.offsetScale.x = 0f;
            lightSpriteRenderer.offsetScale.y = 0f;

            obstacleLength *= percentageLight;
            obstacleWidth *= percentageLight;
            
        }

        public void Hear()
        {
            IEnumerator IsHeardRoutine()
            {
                isHeard = true;
                lightSpriteRenderer.DotweenTo(size * percentageLight, delay);
                yield return new WaitForSeconds(delay);
                lightSpriteRenderer.DotweenTo(Vector2.zero, delay);
                isHeard = false;
            }

            if (!isHeard)
            {
                StartCoroutine(IsHeardRoutine());
            }
        }

    }
}