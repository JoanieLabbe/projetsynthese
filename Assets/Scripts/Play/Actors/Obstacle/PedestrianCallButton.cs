﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class PedestrianCallButton : MonoBehaviour, IInteractable
    {
        [SerializeField] private float timeActiveInSecond = 5;

        private PedestrianCrossingStimulus pedestrianCrossingStimulu;
        private PedestrianCrossingEventChannel pedestrianCrossingEventChannel;

        private void Awake()
        {
            pedestrianCrossingStimulu = GetComponentInParent<PedestrianCrossingStimulus>();
            pedestrianCrossingEventChannel = Finder.PedestrianCrossingEventChannel;
            pedestrianCrossingEventChannel.OnPedestrianWaitLimit += OnPedestrianWaitLimit;
        }

        private void OnDestroy()
        {
            pedestrianCrossingEventChannel.OnPedestrianWaitLimit -= OnPedestrianWaitLimit;
        }

        private void OnPedestrianWaitLimit()
        {
            if (!pedestrianCrossingStimulu.IsActive)
            {
                ActivePedestrianCrossing();
            }
        }

        public void StartInteraction(Player sender)
        {
            if (!pedestrianCrossingStimulu.IsActive)
            {
                ActivePedestrianCrossing();
            }
        }

        public void EndInteraction()
        {
            // Rien à faire
        }

        private void ActivePedestrianCrossing()
        {
            IEnumerator ActivePedestrianCrossingRoutine()
            {
                pedestrianCrossingStimulu.IsActive = true;
                yield return new WaitForSeconds(timeActiveInSecond);
                pedestrianCrossingStimulu.IsActive = false;
            }

            StartCoroutine(ActivePedestrianCrossingRoutine());
        }
    }
}