﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class PedestrianCrossingStimulus : MonoBehaviour
    {
        private PedestrianCrossingEventChannel pedestrianCrossingEventChannel;
        
        private bool isActive;

        public bool IsActive
        {
            get => isActive;
            set
            {
                if (isActive != value)
                {
                    pedestrianCrossingEventChannel.PublishStateChanged(isActive);
                }
                isActive = value;
            }
        }

        private void Awake()
        {
            pedestrianCrossingEventChannel = Finder.PedestrianCrossingEventChannel;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var roadUser = other.gameObject.GetComponent<IRoadUser>();
            if (roadUser != null)
            {
                roadUser.ReceiveRoadSignal(IsActive);
            }
        }
    }
}