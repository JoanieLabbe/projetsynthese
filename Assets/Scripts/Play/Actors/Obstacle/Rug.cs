﻿using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class Rug : MonoBehaviour
    {
        [SerializeField] private float speedReductionFactor = 0.5f;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Player>() != null)
            {
                other.GetComponent<PlayerMover>().Speed *= speedReductionFactor;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<Player>() != null)
            {
                other.GetComponent<PlayerMover>().Speed /= speedReductionFactor;
            }
        }
    }
}