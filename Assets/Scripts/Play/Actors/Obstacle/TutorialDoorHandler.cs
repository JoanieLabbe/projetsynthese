﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class TutorialDoorHandler : MonoBehaviour
    {
        [SerializeField] private AudioClip openDoorNoise;
        
        private DoorAccessRequestedEventChannel doorAccessRequestedEventChannel;
        private CurrentPlayer currentPlayer;
        private SoundPlayer soundPlayer;

        private void Awake()
        {
            currentPlayer = Finder.CurrentPlayer;
            soundPlayer = Finder.SoundPlayer;
            doorAccessRequestedEventChannel = Finder.DoorAccessRequestedEventChannel;
            doorAccessRequestedEventChannel.OnDoorAccessRequested += OnTutorialDoorAccessRequested;
        }

        private void OnDestroy()
        {
            doorAccessRequestedEventChannel.OnDoorAccessRequested -= OnTutorialDoorAccessRequested;
        }

        private void OnTutorialDoorAccessRequested(GameObject door)
        {
            if (currentPlayer.HasDog && gameObject == door)
            {
                door.SetActive(false);
                soundPlayer.PlaySound(openDoorNoise, 0.35f);
            }
        }
    }
}