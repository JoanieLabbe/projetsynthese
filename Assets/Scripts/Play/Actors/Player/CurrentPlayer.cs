﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author: Carol-Ann Collin
    [Findable(Tags.LevelController)]
    public class CurrentPlayer : MonoBehaviour
    {
        private PlayerChangeEventChannel playerChangeEventChannel;
        
        public Player Player { get; set; }

        public bool HasDog { get; private set; }
        
        private void Awake()
        {
            playerChangeEventChannel = Finder.PlayerChangeEventChannel;
            playerChangeEventChannel.OnPlayerChange += OnPlayerChange;
            playerChangeEventChannel.Publish(Finder.Player);
        }

        private void OnDestroy()
        {
            playerChangeEventChannel.OnPlayerChange -= OnPlayerChange;
        }
        
        private void OnPlayerChange(Player player)
        {
            Player = player;
            HasDog = player.gameObject.GetComponentInChildren<Dog>() != null;
        }
    }
}