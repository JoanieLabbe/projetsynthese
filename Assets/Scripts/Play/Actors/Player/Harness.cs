﻿using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class Harness : MonoBehaviour
    {
        private Dog dogLight;

        private void Awake()
        {
            dogLight = GetComponentInChildren<Dog>();
        }

        public void Rotate (Vector2 direction)
        {
            transform.up = direction;
        }

        public void ToggleDogLightSource (bool isEnabled)
        {
            dogLight.ToggleLightSource(isEnabled);
        }
    }
}