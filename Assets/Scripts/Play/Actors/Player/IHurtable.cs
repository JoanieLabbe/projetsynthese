﻿namespace Game
{
    // Author : Joanie Labbé
    public interface IHurtable
    {
        void Hurt();
    }
}