﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class Checklist : MonoBehaviour
    {
        private ChecklistEventChannel eventChannel;
        private Image[] checks;

        private void Awake()
        {
            eventChannel = Finder.ChecklistEventChannel;
            eventChannel.OnChecklistChecked += VerifyWhichZoneToCheck;
            eventChannel.OnChecklistReseted += UncheckZones;
            checks = GetComponentsInChildren<Image>();
        }

        private void OnDestroy()
        {
            eventChannel.OnChecklistChecked -= VerifyWhichZoneToCheck;
            eventChannel.OnChecklistReseted -= UncheckZones;
        }

        private void VerifyWhichZoneToCheck(Level4Zones zone)
        {
            switch (zone)
            {
                case Level4Zones.PedestrianCrossing:
                    CheckZone(0);
                    break;
                case Level4Zones.Construction:
                    CheckZone(1);
                    break;
                case Level4Zones.TopRoad:
                    CheckZone(2);
                    break;
                case Level4Zones.SmallAlley:
                    CheckZone(3);
                    break;
            }
        }
        
        private void CheckZone(int index)
        {
            checks[index].enabled = true;
        }

        private void UncheckZones()
        {
            foreach (var image in checks)
            {
                image.enabled = false;
            }
        }
    }
}