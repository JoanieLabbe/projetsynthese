﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class ChecklistEnabler : MonoBehaviour
    {
        private ObjectivesEventChannel eventChannel;
        private Canvas checklistCanvas;

        private void Awake()
        {
            eventChannel = Finder.ObjectivesEventChannel;
            eventChannel.OnObjectiveCompleted += EnableChecklist;
            checklistCanvas = GameObject.FindWithTag(Tags.Checklist).GetComponent<Canvas>();
        }
        
        private void OnDestroy()
        {
            eventChannel.OnObjectiveCompleted -= EnableChecklist;
            if (checklistCanvas != null)
            {
                checklistCanvas.enabled = false;
            }
        }

        private void EnableChecklist()
        {
            checklistCanvas.enabled = true;
        }
        
    }
}