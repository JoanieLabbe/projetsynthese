﻿using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault, Vivianne Lord
    public class ChecklistLanguage : MonoBehaviour
    {
        [Header("Languages")]
        [SerializeField] private LanguageTemplate french;
        [SerializeField] private LanguageTemplate english;
        [SerializeField] private LanguageTemplate norwegian;

        private OptionsController optionsController;
        private LanguageEventChannel languageEventChannel;
        
        private TextMeshProUGUI[] zones;
        private string[] labels;
        
        private void Awake()
        {
            optionsController = Finder.OptionsController;
            
            languageEventChannel = Finder.LanguageEventChannel;
            languageEventChannel.OnLanguageChangeEvent += ChangeActiveLanguage;
            
            SetActiveLanguage();
            zones = GetComponentsInChildren<TextMeshProUGUI>();
            int index = 0;
            foreach (var t in zones)
            {
                t.text = labels[index];
                index++;
            }
        }

        private void OnDestroy()
        {
            languageEventChannel.OnLanguageChangeEvent -= ChangeActiveLanguage;
        }

        private void SetActiveLanguage()
        {
            switch (optionsController.Values.language)
            {
                case Languages.FR:
                  SetFrench();
                  break;
                case Languages.ENG:
                    SetEnglish();
                    break;
                case Languages.NOR:
                    SetNorwegian();
                    break;
            }
        }

        private void ChangeActiveLanguage()
        {
            SetActiveLanguage();

            int index = 0;
            foreach (var t in zones)
            {
                t.text = labels[index];
                index++;
            }
        }

        private void SetFrench()
        {
            labels = french.checklistZones;
        }

        private void SetEnglish()
        {
            labels = english.checklistZones;
        }

        private void SetNorwegian()
        {
            labels = norwegian.checklistZones;
        }
    }
}