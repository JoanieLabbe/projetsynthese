﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class PlayerLight : MonoBehaviour
    {
        private LightingSource2D lightingSource2D;

        public float LightSize
        {
            get => lightingSource2D.lightSize;
            set => lightingSource2D.lightSize = value;
        }

        private void Awake()
        {
            lightingSource2D = GetComponent<LightingSource2D>();
        }
    }
}