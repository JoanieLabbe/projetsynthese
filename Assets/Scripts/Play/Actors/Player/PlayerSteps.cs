﻿using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class PlayerSteps : MonoBehaviour
    {
        private AudioSource stepsAudio;

        public bool IsWalking { get; set; }

        private void Awake()
        {
            stepsAudio = GetComponent<AudioSource>();
        }

        private void Update()
        {
            stepsAudio.mute = !IsWalking;
        }
    }
}