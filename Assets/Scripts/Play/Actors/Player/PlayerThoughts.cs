﻿using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class PlayerThoughts : MonoBehaviour
    {
        private PlayerThoughtsLanguageModel languageModel;
        
        private TextMeshProUGUI thoughtsText;
        private string[] thoughts;
        private PlayerIsThinkingEventChannel eventChannel;
        private LanguageEventChannel languageEventChannel;

        private Thought currentThought;
        private int currentWaitTime;
        
        private void Awake()
        {
            languageModel = new PlayerThoughtsLanguageModel();
            SetActiveLanguage();
            
            thoughts = languageModel.Thoughts;
            thoughtsText = GetComponent<TextMeshProUGUI>();
            thoughtsText.enabled = false;

            eventChannel = Finder.PlayerIsThinkingEventChannel;
            eventChannel.OnPlayerThinking += MakeTextAppearForSeconds;

            languageEventChannel = Finder.LanguageEventChannel;
            languageEventChannel.OnLanguageChangeEvent += UpdateLanguage;
        }

        private void OnDestroy()
        {
            eventChannel.OnPlayerThinking -= MakeTextAppearForSeconds;
            languageEventChannel.OnLanguageChangeEvent -= UpdateLanguage;
        }

        private void MakeTextAppearForSeconds(Thought thoughtToDisplay, int waitTimeInSeconds)
        {
            if (!thoughtsText.enabled)
            {
                StartCoroutine(Routine(thoughtToDisplay, waitTimeInSeconds));
                currentThought = thoughtToDisplay;
                currentWaitTime = waitTimeInSeconds;
            }
        }

        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }

        // Author : Vivianne Lord
        private void UpdateLanguage()
        {
            SetActiveLanguage();
            thoughts = languageModel.Thoughts;
            if (thoughtsText.enabled)
            {
                StartCoroutine(Routine(currentThought, currentWaitTime));
            }
        }
        
        IEnumerator Routine(Thought thoughtToDisplay, int waitTimeInSeconds)
        {
            thoughtsText.text = thoughts[(int)thoughtToDisplay];
            thoughtsText.enabled = true;
            yield return new WaitForSeconds(waitTimeInSeconds);
            thoughtsText.enabled = false;
        }
    }
}