﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class PlayerThoughtsLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] Thoughts { get; private set; }

        public PlayerThoughtsLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Thoughts = languageAssets.FrenchAsset.thoughts;
        }

        public void SetEnglish()
        {
            Thoughts = languageAssets.EnglishAsset.thoughts;
        }

        public void SetNorwegian()
        {
            Thoughts = languageAssets.NorwegianAsset.thoughts;
        }
    }
}