﻿namespace Game
{
    public enum Thought
    {
        Level4Intro,
        Further,
        LessFar,
        OtherSide,
        There
    }
}