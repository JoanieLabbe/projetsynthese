﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class PlayerSonar : MonoBehaviour
    {
        private PlayerMover playerMover;

        private void Awake()
        {
            playerMover = GetComponent<PlayerMover>();
        }

        public void SpawnSonar()
        {
            GameObject gameObject = ObjectPooler.EnableObject(Tags.Sonar, transform.position, Quaternion.identity);
            if (gameObject != null)
            {
                gameObject.GetComponent<Sonar>().PlayerSpeed = playerMover.Speed;
            }
        }
    }
}