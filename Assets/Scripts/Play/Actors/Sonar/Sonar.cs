﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class Sonar : Recyclable
    {
        [SerializeField] private float maxRadius = 5f;
        [SerializeField] private float speed = 2f;
        private const int DIVIDER = 50;
        
        private float minRadius = 0f;

        private float radius;
        private CircleCollider2D circleCollider2D;
        private LightingSource2D lightingSource2D;
        
        public float PlayerSpeed { get; set; }

        private float Radius
        {
            get => radius;
            set
            {
                radius = value;
                if (circleCollider2D != null)
                {
                    circleCollider2D.radius = radius;
                }
                if (lightingSource2D != null)
                {
                    lightingSource2D.lightSize = radius * 2;
                }
            }
        }

        private void Start ()
        {
            circleCollider2D = gameObject.GetComponent<CircleCollider2D>();
            lightingSource2D = gameObject.GetComponent<LightingSource2D>();
        }

        private void OnEnable()
        {
            Radius = minRadius;
        }

        private void Update()
        {
            Radius += PlayerSpeed/DIVIDER * speed * Time.deltaTime;
            if (Radius >= maxRadius)
            {
                Radius = minRadius;
                Recycle();
            }
        }
    }
}