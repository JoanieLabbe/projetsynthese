﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.Audio;

namespace Game
{
    // Author : Joanie Labbé, Carol-Ann Collin
    public class AtmosphereSound : Recyclable
    {
        [Header("Sounds")]
        [SerializeField][Range(0,1)] private float maxSoundVolume = 1f;
        [SerializeField][Range(0,1)] private float minSoundVolume = 0.01f;
        [Range(0,Single.MaxValue)]
        [SerializeField] private float distanceFromCenterForMaxSound = 1f;
        [SerializeField] private AudioClip audioClip;
        [SerializeField] private bool loopAudio = true;
        [SerializeField] private float loopDelay;

        private const string AUDIOMIXER_FILE_NAME = "GameSounds";
        private const string AUDIOMIXER_SUBFILE_NAME = "Environment";

        private PlayerChangeEventChannel playerChangeEventChannel;
        private AudioSource audioSource;
        private Player player;
        
        private float MaxVolume => maxSoundVolume;

        public bool IsPlaying { get; private set; }

        private void Awake()
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            playerChangeEventChannel = Finder.PlayerChangeEventChannel;
            playerChangeEventChannel.OnPlayerChange += OnPlayerChange;
            
            SetAudioMixer();
            audioSource.clip = audioClip;
            audioSource.volume = minSoundVolume == 0 ? 0.01f : minSoundVolume;
            IsPlaying = false;
            StartSound();
        }

        private void Start()
        {
            player = Finder.CurrentPlayer.Player;
        }

        private void OnDestroy()
        {
            playerChangeEventChannel.OnPlayerChange -= OnPlayerChange;
        }

        private void Update()
        {
            float distance = Math.Abs((player.transform.position - transform.position).magnitude);
            audioSource.volume = Mathf.Lerp(audioSource.volume,
                distance <= distanceFromCenterForMaxSound
                    ? MaxVolume
                    : MaxVolume * distanceFromCenterForMaxSound / distance, Time.deltaTime);
        }
        
        private void OnPlayerChange(Player player)
        {
            this.player = player;
        }

        public void StartSound()
        {
            IEnumerator CouroutineEndAudioClip()
            {
                StartPlayingSound();
                yield return new WaitForSeconds(audioSource.clip.length);
                StopSound();
            }

            if (!IsPlaying && audioClip != null)
            {
                StartCoroutine(CouroutineEndAudioClip());
            }
        }
        
        private void StartSoundWithDelay()
        {
            IEnumerator CouroutineEndAudioClip()
            {
                yield return new WaitForSeconds(loopDelay);
                StartSound();
            }

            if (!IsPlaying)
            {
                StartCoroutine(CouroutineEndAudioClip());
            }
        }

        private void StartPlayingSound()
        {
            if (audioClip != null)
            {
                IsPlaying = true;
                audioSource.Play();
            }
            else
            {
                IsPlaying = false;
            }
        }

        public void StopSound()
        {
            audioSource.Stop();
            IsPlaying = false;
            if (loopAudio)
            {
                StartSoundWithDelay();
            }
        }

        // Author : Kamylle Thériault
        private void SetAudioMixer()
        {
            AudioMixer audioMixer = Resources.Load<AudioMixer>(AUDIOMIXER_FILE_NAME);
            AudioMixerGroup[] audioMixerGroup = audioMixer.FindMatchingGroups(AUDIOMIXER_SUBFILE_NAME);
            audioSource.outputAudioMixerGroup = audioMixerGroup[0];
        }
    }
}