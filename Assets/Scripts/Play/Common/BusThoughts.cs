﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class BusThoughts : MonoBehaviour
    {
        [SerializeField] private Thought thoughtToDisplay;
        
        [Header("Wait time")]
        [SerializeField] private int waitTimeInSeconds = 2;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                Finder.PlayerIsThinkingEventChannel.Publish(thoughtToDisplay, waitTimeInSeconds);
            }
        }
    }
}