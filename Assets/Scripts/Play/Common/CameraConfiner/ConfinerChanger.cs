﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class ConfinerChanger : MonoBehaviour
    {
        [SerializeField] private ConfinerItem defaultConfiner;
        private ZoneChangeEventChannel zoneChangeEventChannel;
        private PolygonCollider2D polygonCollider;
        
        private void Awake()
        {
            zoneChangeEventChannel = Finder.ZoneChangeEventChannel;
            zoneChangeEventChannel.OnConfinerChange += OnConfinerChange;

            polygonCollider = GetComponent<PolygonCollider2D>();
            
            zoneChangeEventChannel.PublishConfinerChange(defaultConfiner);
        }

        private void OnDestroy()
        {
            zoneChangeEventChannel.OnConfinerChange -= OnConfinerChange;
        }

        private void OnConfinerChange(ConfinerItem confiner)
        {
            polygonCollider.points = confiner.Confiner.points;
            transform.position = confiner.Position;
        }
    }
}