﻿using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class ConfinerChecker : MonoBehaviour
    {
        private ZoneChangeEventChannel zoneChangeEventChannel;
        private LevelConfiners levelConfiners;
        
        private void Start()
        {
            zoneChangeEventChannel = Finder.ZoneChangeEventChannel;
            zoneChangeEventChannel.OnZoneChange += OnZoneChange;

            levelConfiners = Finder.LevelConfiners;
        }

        private void OnDestroy()
        {
            zoneChangeEventChannel.OnZoneChange -= OnZoneChange;
        }

        private void OnZoneChange(string nextPlaceName)
        {
            foreach (var item in levelConfiners.Confiners.Where(item => item.PlaceName == nextPlaceName.Substring(2)))
            {
                zoneChangeEventChannel.PublishConfinerChange(item);
            }
        }
    }
}