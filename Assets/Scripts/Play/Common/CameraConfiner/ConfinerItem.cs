﻿using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [CreateAssetMenu(fileName = "New Confiner Item", menuName = "Game/Confiner")]
    public class ConfinerItem : ScriptableObject
    {
        [SerializeField] private string placeName;
        [SerializeField] private PolygonCollider2D confiner;
        [SerializeField] private Vector2 position;
        
        public PolygonCollider2D Confiner => confiner;
        public string PlaceName => placeName;
        public Vector2 Position => position;
    }
}