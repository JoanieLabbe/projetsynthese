﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.LevelController)]
    [Serializable]
    public class LevelConfiners : MonoBehaviour
    {
        public List<ConfinerItem> Confiners;
    }
}