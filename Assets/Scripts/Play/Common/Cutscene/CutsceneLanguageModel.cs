﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class CutsceneLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string Button { get; private set; }

        public CutsceneLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Button = languageAssets.FrenchAsset.skipButton;
        }

        public void SetEnglish()
        {
            Button = languageAssets.EnglishAsset.skipButton;
        }

        public void SetNorwegian()
        {
            Button = languageAssets.NorwegianAsset.skipButton;
        }
    }
}