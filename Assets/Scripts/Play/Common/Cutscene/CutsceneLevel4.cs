﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class CutsceneLevel4 : OpeningCutsceneView
    {
        [Header("Wait time")] 
        [SerializeField] private int waitTimeInSeconds = 5;
        
        protected override void DisableComponents()
        {
            base.DisableComponents();
            Finder.PlayerIsThinkingEventChannel.Publish(Thought.Level4Intro, waitTimeInSeconds);
        }
    }
}