﻿using Harmony;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Video;

namespace Game
{
    // Author : Kamylle Thériault
    public class EndCutsceneView : MonoBehaviour
    {
        [SerializeField] private AudioMixer levelSound;
        
        private const string UI_CANVAS_NAME = "UICanvas";
        private const string VOLUME_PARAM_NAME = "masterVolume";
        private const float MIN_DECIBELS = -80f;
        private const float MAX_DECIBELS = 0f;
        
        private FlagUpdates flag;
        private CutsceneEventChannel eventChannel;

        private Camera cutsceneCamera;
        private VideoPlayer video;
        private Canvas uiCanvas;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            eventChannel = Finder.CutsceneEventChannel;
            eventChannel.OnLastCutsceneTriggered += EnableCutscene;

            cutsceneCamera = GetComponentInChildren<Camera>();
            video = GetComponentInChildren<VideoPlayer>();
            video.loopPointReached += DisableCutscene;
            
            uiCanvas = GameObject.Find(UI_CANVAS_NAME).GetComponent<Canvas>();
            
            video.enabled = false;
            cutsceneCamera.enabled = false;
        }

        private void EnableCutscene()
        {
            levelSound.SetFloat(VOLUME_PARAM_NAME, MIN_DECIBELS);
            video.enabled = true;
            cutsceneCamera.enabled = true;
            uiCanvas.enabled = false;
            flag.PlayerShouldUpdate = false;
            flag.EnemyShouldUpdate = false;
            flag.SpawnersShouldUpdate = false;
        }

        private void OnDestroy()
        {
            eventChannel.OnLastCutsceneTriggered -= EnableCutscene;
            video.loopPointReached -= DisableCutscene;
        }

        private void DisableCutscene(VideoPlayer videoPlayer)
        {
            levelSound.SetFloat(VOLUME_PARAM_NAME, MAX_DECIBELS);
            cutsceneCamera.enabled = false;
            uiCanvas.enabled = true;
            flag.PlayerShouldUpdate = true;
            flag.EnemyShouldUpdate = true;
            flag.SpawnersShouldUpdate = true;
            
            Finder.LevelCompleteEventChannel.Publish();
        }
    }
}