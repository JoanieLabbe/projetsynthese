﻿using Harmony;
using UnityEngine;

namespace Game
{
    //author : Kamylle Thériault
    // Classe pour la cinématique de fin
    public class LastCutsceneTrigger : MonoBehaviour
    {
        private CutsceneEventChannel eventChannel;
        private AchievementPublisher achievementPublisher;

        public void Start()
        {
            eventChannel = Finder.CutsceneEventChannel;
            achievementPublisher = Finder.AchievementPublisher;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                achievementPublisher.UnlockAchievement(AchievementType.LevelCompleted);
                eventChannel.PublishLastTrigger();
                gameObject.SetActive(false);
            }
        }
    }
}