﻿using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Game
{
    // Author : Kamylle Thériault
    public class OpeningCutsceneView : MonoBehaviour
    {
        [SerializeField] private AudioMixer levelSound;
        
        private const string UI_CANVAS_NAME = "UICanvas";
        private const string VOLUME_PARAM_NAME = "masterVolume";
        private const float MIN_DECIBELS = -80f;
        private const float MAX_DECIBELS = 0f;
        
        private FlagUpdates flag;
        private CutsceneLanguageModel languageModel;

        private Button skipButton;
        private TextMeshProUGUI[] skipButtonLabels;
        
        private Camera cutsceneCamera;
        private VideoPlayer video;
        
        private Canvas uiCanvas;
        private Canvas cutscene;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            languageModel = new CutsceneLanguageModel();

            levelSound.SetFloat(VOLUME_PARAM_NAME, MIN_DECIBELS);
            cutsceneCamera = GetComponentInChildren<Camera>();
            video = GetComponentInChildren<VideoPlayer>();
            skipButton = GetComponentInChildren<Button>();
            cutscene = GetComponent<Canvas>();
            skipButtonLabels = GetComponentsInChildren<TextMeshProUGUI>();
            
            SetActiveLanguage();
            foreach (var t in skipButtonLabels)
            {
                t.text = languageModel.Button;
            }
            
            video.loopPointReached += DisableCutscene;
            
            uiCanvas = GameObject.Find(UI_CANVAS_NAME).GetComponent<Canvas>();
            uiCanvas.enabled = false;
            flag.PlayerShouldUpdate = false;
            flag.EnemyShouldUpdate = false;
            flag.SpawnersShouldUpdate = false;
            flag.InteractibleDogShouldUpdate = false;
        }

        private void Start()
        {
            skipButton.Select();
        }

        private void OnEnable()
        {
            skipButton.onClick.AddListener(SkipCutscene);
        }

        private void OnDisable()
        {
            skipButton.onClick.RemoveListener(SkipCutscene);
        }

        private void OnDestroy()
        {
            video.loopPointReached -= DisableCutscene;
        }

        private void DisableCutscene(VideoPlayer videoPlayer)
        {
            DisableComponents();
        }

        private void SkipCutscene()
        {
            DisableComponents();
        }

        protected virtual void DisableComponents()
        {
            levelSound.SetFloat(VOLUME_PARAM_NAME, MAX_DECIBELS);
            
            skipButton.enabled = false;
            video.Stop();
            cutscene.enabled = false;
            cutsceneCamera.enabled = false;
            uiCanvas.enabled = true;
            flag.PlayerShouldUpdate = true;
            flag.EnemyShouldUpdate = true;
            flag.SpawnersShouldUpdate = true;
            flag.InteractibleDogShouldUpdate = true;
        }

        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }
    }
}