﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class DrowningZone : MonoBehaviour
    {
        private LevelFailedEventChannel levelFailedEventChannel;
        
        private void Awake()
        {
            levelFailedEventChannel = Finder.LevelFailedEventChannel;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                levelFailedEventChannel.Publish();
            }
        }
    }
}