﻿using UnityEngine;

namespace Game
{
    public class DestinationSelecter : MonoBehaviour
    {
        private PolygonCollider2D zone;
            
        private float minX, maxX, minY, maxY;
            

        public Vector3 FindDestination()
        {
            bool foundDestination = false;

            // Même si nous sommes capable de délimiter la zone permise dans un quadrilatère,
            // puisque la forme de la zone peut être très irrégulière, il faut s'assurer
            // que le point choisi aléatoirement se trouve à l'intérieure de cette zone.
            while (!foundDestination)
            {
                Vector2 point;
                point.x = Random.Range(minX, maxX);
                point.y = Random.Range(minY, maxY);

                if (zone.OverlapPoint(point))
                {
                    return point;
                }
            }
            
            return Vector3.zero;
        }

        public void DetermineMovingLimits(Vector3 startPosition, PolygonCollider2D zone)
        {
            this.zone = zone;
            
            minX = startPosition.x;
            maxX = startPosition.x;
            minY = startPosition.y;
            maxY = startPosition.y;
            
            if (this.zone != null)
            {
                foreach (Vector2 point in this.zone.points)
                {
                    if (point.x <= minX)
                    {
                        minX = point.x;
                    }
                    if (point.x >= maxX)
                    {
                        maxX = point.x;
                    }
                    if (point.y <= minY)
                    {
                        minY = point.y;
                    }
                    if (point.y >= maxY)
                    {
                        maxY = point.y;
                    }
                }
            }
        }
    }
}