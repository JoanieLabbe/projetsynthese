﻿using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class Waypoint : MonoBehaviour
    {
        [SerializeField] private int waitTime;

        public int WaitTime => waitTime;

        public bool IsWaitPoint => waitTime != 0;
    }
}