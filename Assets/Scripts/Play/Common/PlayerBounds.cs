﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class PlayerBounds : MonoBehaviour
    {
        [SerializeField] private float distanceToDeactivate = 2f;
        
        private EdgeCollider2D collider;
        private Collider2D enemyCollider;

        private void Awake()
        {
            collider = GameObject.FindWithTag(Tags.Limit)?.GetComponent<EdgeCollider2D>();
            enemyCollider = GetComponent<Collider2D>();
        }

        private void LateUpdate()
        {
            Vector3 closestPoint = collider.ClosestPoint(gameObject.transform.position);
            if ((closestPoint - gameObject.transform.position).magnitude < distanceToDeactivate)
            {
                enemyCollider.isTrigger = true;
            }
            else
            {
                enemyCollider.isTrigger = false;
            }
        }
    }
}