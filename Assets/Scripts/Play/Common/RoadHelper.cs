﻿using System;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public class RoadHelper : MonoBehaviour
    {
        [SerializeField] private GameObject positionAtStart;
        [SerializeField] private GameObject positionAtEnd;
        [SerializeField] private float speed = 0.25f;
        [SerializeField] private Color lightColor = Color.cyan;
        [SerializeField] private float tolerance = 10f;

        private LightingSource2D lightingSource2D;
        private Vector3 direction;

        private void Awake()
        {
            lightingSource2D = GetComponentInChildren<LightingSource2D>();
            var position = positionAtStart.transform.position;
            lightingSource2D.gameObject.transform.position = position;
            lightingSource2D.lightColor = lightColor;
            direction = Vector3.Normalize(positionAtEnd.transform.position - position);
        }

        private void Update()
        {
            float distance = Math.Abs((lightingSource2D.transform.position - positionAtEnd.transform.position).magnitude);
            if (distance > tolerance)
            {
                lightingSource2D.gameObject.transform.position += (direction * speed);
            }
            else
            {
                lightingSource2D.gameObject.transform.position = positionAtStart.transform.position;
            }
        }
    }
}