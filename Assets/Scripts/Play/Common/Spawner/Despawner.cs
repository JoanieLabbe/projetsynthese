﻿namespace Game
{
    // Author : Joanie Labbe
    public class Despawner : Recyclable
    {
        private PathMover pathMover;

        private void Awake()
        {
            pathMover = GetComponent<PathMover>();
            pathMover.OnPathFinish += OnPathFinish;
        }

        private void OnDestroy()
        {
            pathMover.OnPathFinish -= OnPathFinish;
        }

        private void OnPathFinish()
        {
            Recycle();
        }
    }
}