﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] protected GameObject objectToSpawn;
        [SerializeField] protected List<Waypoint> objectPath;
        [SerializeField] protected bool objectPathRepeat;
        [SerializeField] protected bool shouldDespawn = true;
        protected bool shouldSpawn;

        protected void SpawnEntity()
        {
            GameObject movingActor = ObjectPooler.EnableObject(objectToSpawn.tag, transform.position, Quaternion.identity);
            if (movingActor != null)
            {
                MovingCharacters spawnable = movingActor.GetComponent<MovingCharacters>();
                if (spawnable != null)
                {
                    spawnable.OnSpawn(objectPath, objectPathRepeat, shouldDespawn);
                    shouldSpawn = false;
                }
            }
        }
    }
}