﻿using Harmony;

namespace Game
{
    // Author : Kamylle Thériault
    public class RandomEnemySpawner : EnemySpawner
    {
        public bool ShouldSpawn { get; set; }
        private FlagUpdates flag;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
        }

        private void Update()
        {
            if (ShouldSpawn && flag.SpawnersShouldUpdate)
            {
                SpawnEntity();
                ShouldSpawn = false;
            }
        }
    }
}