﻿using Harmony;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    // Author : Kamylle Thériault
    public class SpawnerRandomizer : MonoBehaviour
    {
        [SerializeField] private int cooldown;

        private FlagUpdates flag;
        private RandomEnemySpawner[] spawners;
        
        private float waitingTime;
        private int lastIndex;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            spawners = GetComponentsInChildren<RandomEnemySpawner>();
            waitingTime = Time.time;
        }

        private void Update()
        {
            if (waitingTime <= Time.time && flag.SpawnersShouldUpdate)
            {
                var index = Random.Range(0, spawners.Length);
                while (index == lastIndex)
                {
                    index = Random.Range(0, spawners.Length);
                }
                spawners[index].ShouldSpawn = true;
                lastIndex = index;
                waitingTime += cooldown;
            }
        }
    }
}