﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class ChecklistTrigger : MonoBehaviour
    {
        [SerializeField] private Level4Zones currentZone;
        
        private ChecklistEventChannel eventChannel;

        public void Start()
        {
            eventChannel = Finder.ChecklistEventChannel;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                eventChannel.PublishSet(currentZone);
                gameObject.SetActive(false);
            }
        }
    }
}