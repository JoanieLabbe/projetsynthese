﻿using Harmony;

namespace Game
{
    // Author : Joanie Labbé
    public class EndLevelTrigger : Recyclable, IInteractable
    {
        private bool isActive = true;
        public void StartInteraction(Player sender)
        {
            if (isActive)
            {
                Finder.LevelCompleteEventChannel.Publish();
                isActive = false;
            }
        }

        public void EndInteraction()
        {
            // Rien à faire
        }
    }
}