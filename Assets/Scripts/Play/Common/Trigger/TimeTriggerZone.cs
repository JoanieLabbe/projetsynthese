﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    public class TimeTriggerZone : Recyclable
    {
        [SerializeField] private float timeToWaitInSecond = 5;

        private bool isPlayerStayInZone;

        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                EndLevelAfterTime();
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                isPlayerStayInZone = false;
            }
        }

        private void EndLevelAfterTime()
        {
            IEnumerator LevelEndRoutine()
            {
                isPlayerStayInZone = true;
                yield return new WaitForSeconds(timeToWaitInSecond);
                if (isPlayerStayInZone)
                {
                    Finder.LevelCompleteEventChannel.Publish();
                    Recycle();
                }
            }

            if (!isPlayerStayInZone)
            {
                StartCoroutine(LevelEndRoutine());
            }
        }
    }
}