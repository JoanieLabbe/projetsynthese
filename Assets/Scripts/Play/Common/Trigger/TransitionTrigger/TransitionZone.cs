﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class TransitionZone : MonoBehaviour
    {
        [SerializeField] private Transform destination;
        [SerializeField] private bool confinerChangeIsRequired;

        private ZoneChangeEventChannel zoneChangeEventChannel;
   
        private void Awake()
        {
            if (confinerChangeIsRequired)
            {
                zoneChangeEventChannel = Finder.ZoneChangeEventChannel;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                other.transform.parent.position = destination.transform.position;
                if (confinerChangeIsRequired)
                {
                    zoneChangeEventChannel.PublishZoneChange(destination.name);
                }
            }
        }
    }
}