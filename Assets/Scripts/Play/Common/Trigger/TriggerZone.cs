﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class TriggerZone : MonoBehaviour
    {
        private LevelCompleteEventChannel levelCompleteEventChannel;

        public void Start()
        {
            levelCompleteEventChannel = Finder.LevelCompleteEventChannel;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                levelCompleteEventChannel.Publish();
                gameObject.SetActive(false);
            }
        }
    }
}