﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.GameController)]
    public class AchievementEventChannel : MonoBehaviour
    {
        public event AchievementUnlockedEvent OnAchievementUnlocked;

        public void Publish(AchievementType achievementType, DateTime dateTime)
        {
            if (OnAchievementUnlocked != null)
            {
                OnAchievementUnlocked(achievementType, dateTime);
            }
        }
    }
    
    public delegate void AchievementUnlockedEvent(AchievementType achievementType, DateTime dateTime);
}