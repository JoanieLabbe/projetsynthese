﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class ChecklistEventChannel : MonoBehaviour
    {
        public event ChecklistCheckedEvent OnChecklistChecked;
        public event ChecklistResetEvent OnChecklistReseted;
        
        public void PublishSet(Level4Zones zone)
        {
            if (OnChecklistChecked != null)
            {
                OnChecklistChecked(zone);
            }
        }
        
        public void PublishReset()
        {
            if (OnChecklistReseted != null)
            {
                OnChecklistReseted();
            }
        }
    }
    
    public delegate void ChecklistCheckedEvent(Level4Zones zone);
    public delegate void ChecklistResetEvent();
}