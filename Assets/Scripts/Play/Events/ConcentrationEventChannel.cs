﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class ConcentrationEventChannel : MonoBehaviour
    {
        public event ConcentrationDepletedEvent OnConcentrationDepleted;
        public event ConcentrationStartedEvent OnConcentrationStarted;
        public event ConcentrationStoppedEvent OnConcentrationStopped;
        
        public void Publish(bool isEmpty)
        {
            if (OnConcentrationDepleted != null)
            {
                OnConcentrationDepleted(isEmpty);
            }
        }

        public void PublishActivation()
        {
            if (OnConcentrationStarted != null)
            {
                OnConcentrationStarted();
            }
        }

        public void PublishDeactivation()
        {
            if (OnConcentrationStopped != null)
            {
                OnConcentrationStopped();
            }
        }
    }
    
    public delegate void ConcentrationDepletedEvent(bool isEmpty);
    public delegate void ConcentrationStartedEvent();
    public delegate void ConcentrationStoppedEvent();
}