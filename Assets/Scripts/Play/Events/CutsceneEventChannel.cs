﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class CutsceneEventChannel : MonoBehaviour
    {
        public event LastCutsceneTriggeredEvent OnLastCutsceneTriggered;
        public event RegularCutsceneTriggeredEvent OnCutsceneTriggered;

        public void PublishLastTrigger()
        {
            if (OnLastCutsceneTriggered != null)
            {
                OnLastCutsceneTriggered();
            }
        }
        
        public void PublishRegularTrigger()
        {
            if (OnCutsceneTriggered != null)
            {
                OnCutsceneTriggered();
            }
        }
    }
    
    public delegate void LastCutsceneTriggeredEvent();
    public delegate void RegularCutsceneTriggeredEvent();
}