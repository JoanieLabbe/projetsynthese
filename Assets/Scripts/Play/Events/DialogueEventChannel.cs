﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class DialogueEventChannel : MonoBehaviour
    {
        public event DialogueIsActiveEvent OnDialogueStarted;

            public void Publish()
            {
                if (OnDialogueStarted != null)
                {
                    OnDialogueStarted();
                }
            }
    }
    
    public delegate void DialogueIsActiveEvent();
}