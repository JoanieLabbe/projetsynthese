﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.LevelController)]
    public class DoorAccessRequestedEventChannel : MonoBehaviour
    {
        public event DoorAccessRequestedEvent OnDoorAccessRequested;
        
        public void Publish(GameObject door)
        {
            if (OnDoorAccessRequested != null)
            {
                OnDoorAccessRequested(door);
            }
        }
    }
    
    public delegate void DoorAccessRequestedEvent(GameObject door);
}