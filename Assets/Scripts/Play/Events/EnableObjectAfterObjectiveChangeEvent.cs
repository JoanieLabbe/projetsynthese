﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    public class EnableObjectAfterObjectiveChangeEvent : MonoBehaviour
    {
        [SerializeField] private GameObject objectToSetActive;
        
        private ObjectivesEventChannel objectivesEventChannel;

        private void Awake()
        {
            objectivesEventChannel = Finder.ObjectivesEventChannel;
            objectivesEventChannel.OnObjectiveCompleted += OnObjectiveChange;
        }

        private void OnDestroy()
        {
            objectivesEventChannel.OnObjectiveCompleted -= OnObjectiveChange;
        }

        private void OnObjectiveChange()
        {
            if (objectToSetActive != null)
            {
                objectToSetActive.SetActive(true);
            }
        }
    }
}