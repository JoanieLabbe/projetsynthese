﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.MainController)]
    public class InventoryEventChannel : MonoBehaviour
    {
        public event NewItemAcquiredEvent OnItemAcquired;
        public event ItemRemovedEvent OnItemRemoved;

        public void PublishItemAcquired(InventoryItem inventoryItem)
        {
            if (OnItemAcquired != null)
            {
                OnItemAcquired(inventoryItem);
            }
        }

        public void PublishItemRemoved(InventoryItem inventoryItem)
        {
            if (OnItemRemoved != null)
            {
                OnItemRemoved(inventoryItem);
            }
        }
        
    }
    
    public delegate void NewItemAcquiredEvent(InventoryItem inventoryItem);
    public delegate void ItemRemovedEvent(InventoryItem inventoryItem);
}