﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    [Findable(Tags.MainController)]
    public class LanguageEventChannel : MonoBehaviour
    {
        public event LanguageChangeEvent OnLanguageChangeEvent;

        public void Publish()
        {
            if (OnLanguageChangeEvent != null)
            {
                OnLanguageChangeEvent();
            }
        }
    }
    
    public delegate void LanguageChangeEvent();
}