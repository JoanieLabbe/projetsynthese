﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Marc-Antoine Sigouin
    [Findable(Tags.MainController)]
    public class LevelFailedEventChannel : MonoBehaviour
    {
        public event LevelFailedEvent OnLevelFailed;

        public void Publish()
        {
            if (OnLevelFailed != null)
            {
                OnLevelFailed();
            }
        }
    }

    public delegate void LevelFailedEvent();
}