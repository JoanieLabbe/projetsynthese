﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.MainController)]
    public class LevelLoadedEventChannel : MonoBehaviour
    {
        public event LevelLoadedEvent OnLevelLoaded;

        public void Publish(int level)
        {
            if (OnLevelLoaded != null)
            {
                OnLevelLoaded(level);
            }
        }
    }
    
    public delegate void LevelLoadedEvent(int level);
}