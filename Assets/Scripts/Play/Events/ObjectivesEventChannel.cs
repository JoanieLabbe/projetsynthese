﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class ObjectivesEventChannel : MonoBehaviour
    {
        public event ObjectiveIsCompletedEvent OnObjectiveCompleted;
        public event ObjectiveIsResetedEvent OnObjectiveReset;

        public void PublishCompleted()
        {
            if (OnObjectiveCompleted != null)
            {
                OnObjectiveCompleted();
            }
        }
        
        public void PublishReseted()
        {
            if (OnObjectiveReset != null)
            {
                OnObjectiveReset();
            }
        }
    }
    
    public delegate void ObjectiveIsCompletedEvent();
    public delegate void ObjectiveIsResetedEvent();
}