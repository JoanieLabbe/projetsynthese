﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    [Findable(Tags.LevelController)]
    public class PedestrianCrossingEventChannel : MonoBehaviour
    {
        public event OnWaitLimitExceeded OnPedestrianWaitLimit;
        public event PedestrianCrossingChangeStateEvent OnStateChange;

        public void PublishWaitLimitExceeded()
        {
            if (OnPedestrianWaitLimit != null)
            {
                OnPedestrianWaitLimit();
            }
        }

        public void PublishStateChanged(bool isActive)
        {
            if (OnStateChange != null)
            {
                OnStateChange(isActive);
            }
        }
    }
    
    public delegate void OnWaitLimitExceeded();
    public delegate void PedestrianCrossingChangeStateEvent(bool isActive);
}