﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.LevelController)]
    public class PlayerChangeEventChannel : MonoBehaviour
    {
        public event PlayerChangeEvent OnPlayerChange;

        public void Publish(Player player)
        {
            if (OnPlayerChange != null)
            {
                OnPlayerChange(player);
            }
        }
    }
    
    public delegate void PlayerChangeEvent(Player player);
}