﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class PlayerIsThinkingEventChannel : MonoBehaviour
    {
        public event IsThinkingEvent OnPlayerThinking;

        public void Publish(Thought thoughtToDisplay, int waitTimeInSeconds)
        {
            if (OnPlayerThinking != null)
            {
                OnPlayerThinking(thoughtToDisplay, waitTimeInSeconds);
            }
        }
    }
    
    public delegate void IsThinkingEvent(Thought thoughtToDisplay, int waitTimeInSeconds);
}