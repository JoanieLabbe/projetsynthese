﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.LevelController)]
    public class WalletEventChannel : MonoBehaviour
    {
        public event WalletLostEvent OnWalletLost;
        public event ElevatorExitEvent OnElevatorExit;

        public void ChangePosition(Vector2 position)
        {
            if (OnWalletLost != null)
            {
                OnWalletLost(position);
            }
        }
        
        public void ShowCoworker()
        {
            if (OnElevatorExit != null)
            {
                OnElevatorExit();
            }
        }
    }

    public delegate void ElevatorExitEvent();
    public delegate void WalletLostEvent(Vector2 position);
}