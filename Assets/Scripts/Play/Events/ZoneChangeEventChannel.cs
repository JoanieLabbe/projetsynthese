﻿using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.LevelController)]
    public class ZoneChangeEventChannel : MonoBehaviour
    {
        public event ZoneChangeEvent OnZoneChange;
        public event ConfinerChangeEvent OnConfinerChange;

        public void PublishZoneChange(string nextPlaceName)
        {
            if (OnZoneChange != null)
            {
                OnZoneChange(nextPlaceName);
            }
        }

        public void PublishConfinerChange(ConfinerItem confiner)
        {
            if (OnConfinerChange != null)
            {
                OnConfinerChange(confiner);
            }
        }
    }
    
    public delegate void ZoneChangeEvent(string nextPlaceName);
    public delegate void ConfinerChangeEvent(ConfinerItem confiner);
}