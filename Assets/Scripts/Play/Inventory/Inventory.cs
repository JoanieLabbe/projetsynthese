﻿using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin et Joanie Labbé
    [Findable(Tags.LevelController)]
    public class Inventory : MonoBehaviour
    {
        private InventoryEventChannel inventoryEventChannel;
        private readonly List<Item> itemsInInventory = new List<Item>();
        
        private void Awake()
        {
            inventoryEventChannel = Finder.InventoryEventChannel;
        }

        private void OnDestroy()
        {
            foreach (Item item in itemsInInventory)
            {
                inventoryEventChannel.PublishItemRemoved(item.InventoryItem);
            }
        }

        public void IncreaseQuantity(InventoryItem inventoryItem)
        {
            GetItem(inventoryItem).Quantity += 1;
            inventoryEventChannel.PublishItemAcquired(inventoryItem);
        }

        private Item GetItem(InventoryItem inventoryItem)
        {
            foreach (Item item in itemsInInventory.Where(item => item.InventoryItem.Tag == inventoryItem.Tag))
            {
                return item;
            }

            Item newItem = new Item(inventoryItem);
            itemsInInventory.Add(newItem);
            
            return newItem;
        }
        
        public Item GetItemByTag(string itemTag)
        {
            return itemsInInventory.FirstOrDefault(item => item.InventoryItem.Tag == itemTag);
        }

        public int GetQuantity(InventoryItem inventoryItem)
        {
            return GetItem(inventoryItem).Quantity;
        }
    }
}