﻿using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé
    [CreateAssetMenu(fileName = "New Inventory Item", menuName = "Game/Inventory Item")]
    public class InventoryItem : ScriptableObject
    {
        [SerializeField] private Sprite sprite;
        [SerializeField] private string tag;

        public Sprite Sprite => sprite;
        public string Tag => tag;
    }
}