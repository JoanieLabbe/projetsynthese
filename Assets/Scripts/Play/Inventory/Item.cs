﻿namespace Game
{
    // Author : Carol-Ann Collin
    public class Item
    {
        public int Quantity { get; set; }
        public InventoryItem InventoryItem { get; }

        public Item(InventoryItem inventoryItem)
        {
            InventoryItem = inventoryItem;
        }
    }
}