﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    [Findable(Tags.MainController)]
    public class LanguageAssets : MonoBehaviour
    {
        [SerializeField] private LanguageTemplate frenchAsset;
        [SerializeField] private LanguageTemplate englishAsset;
        [SerializeField] private LanguageTemplate norwegianAsset;

        public LanguageTemplate FrenchAsset => frenchAsset;
        public LanguageTemplate EnglishAsset => englishAsset;
        public LanguageTemplate NorwegianAsset => norwegianAsset;
    }
}