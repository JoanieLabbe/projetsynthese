﻿using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [CreateAssetMenu(fileName = "New Language", menuName = "Language")]
    public class LanguageTemplate : ScriptableObject
    {
        [Header("File selection text")]
        public string[] fileSelectionLabels;
        public string[] fileTitles;
        public string[] fileSelectionButtons;

        [Header("Home text")]
        public string[] homeLabels;
        public string[] homeButtons;

        [Header("Level selection text")]
        public string[] levelSelectionLabels;
        public string[] levelNames;
        public string[] levelStatus;
        public string[] levelSelectionButtons;

        [Header("Achievements Menu")]
        public string[] achievementName;
        public string[] achievementDescription;
        public string[] achievementStatus;
        public string Title;
        public string backButton;
        
        [Header("Options text")]
        public string[] optionsLabels;
        public string[] optionsButtons;
        
        [Header("Pause text")]
        public string[] pauseLabels;
        public string[] pauseButtons;
        
        [Header("HUD text")]
        public string[] objectiveLevel1Labels;
        public string[] objectiveLevel2Labels;
        public string[] objectiveLevel3Labels;
        public string[] objectiveLevel4Labels;
        public string[] objectiveLevel5Labels;

        [Header("Cutscene")] 
        public string skipButton;

        [Header("Tutorial text")] 
        public string[] tutorialLabels;
        
        [Header("Dialogue text")]
        public string[] level4Dialogue;
        public string continueButton;

        [Header("Thoughts Labels")] 
        public string[] thoughts;
        public string[] checklistZones;
    }
}