﻿namespace Game
{
    // Author : Kamylle Thériault
    public enum Languages
    {
        FR,
        ENG,
        NOR
    }
}