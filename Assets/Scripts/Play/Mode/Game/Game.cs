using System;
using Harmony;
using UnityEngine;

namespace Game
{
    public class Game : MonoBehaviour
    {
        private const float TOLERANCE = 0.00001f;
        
        private Main main;
        private FlagUpdates flag;
        private InputActions.GameActions gameInputs;

        private void Awake()
        {
            main = Finder.Main;
            flag = Finder.FlagUpdates;
            gameInputs = Finder.Inputs.Actions.Game;
        }

        private void Start()
        {
            gameInputs.Enable();
            LaunchGame();
        }

        // Author : Kamylle Thériault
        private void Update()
        {
            if (flag.PlayerShouldUpdate)
            {
                if (gameInputs.Exit.triggered)
                {
                    Time.timeScale = 0;
                    gameInputs.Disable();
                }
                if (Math.Abs(Time.timeScale - 1f) < TOLERANCE && !gameInputs.enabled)
                {
                    gameInputs.Enable();
                }
            }
        }

        // Author : Marc-Antoine Sigouin
        private void LaunchGame()
        {
            main.LoadLevel();
        }
        
        // Author : Marc-Antoine Sigouin
        private void OnDestroy()
        {
            main.UnloadCurrentLoadedLevel();

            if (Math.Abs(Time.timeScale - 1f) > TOLERANCE)
            {
                Time.timeScale = 1f;
            }
        }
    }
}