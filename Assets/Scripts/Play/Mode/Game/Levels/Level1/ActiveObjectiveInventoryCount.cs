﻿using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class ActiveObjectiveInventoryCount : MonoBehaviour
    {
        [SerializeField] private List<InventoryItem> itemsToCheck;

        private Inventory inventory;
        
        private void Awake()
        {
            inventory = Finder.Inventory;
        }

        private void Update()
        {
            List<Item> currentItems = new List<Item>();
            itemsToCheck.ForEach(item => currentItems.Add(inventory.GetItemByTag(item.Tag)));

            if (currentItems.All(item => item != null))
            {
                Finder.ObjectivesEventChannel.PublishCompleted();
                enabled = false;
            }
        }
    }
}