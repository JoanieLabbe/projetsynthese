﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class BrushZone : MonoBehaviour
    {
        [SerializeField] private Dragable toothbrush;
        [SerializeField] private float brushTime = 3f;
        [Header("Sound")] 
        [SerializeField] private AudioClip brushingNoise;

        private Vector2 startPosition;
        // Empêche la coroutine de se faire appeler chaque "frame" lorsque l'action est déjà en cours
        private bool brushing;
        // Empêche l'accomplissement de se débloquer une seconde fois
        private bool teethBrushed;
        
        private AchievementPublisher achievementPublisher;
        private SoundPlayer soundPlayer;
        
        private void Awake()
        {
            startPosition = toothbrush.transform.position;
            achievementPublisher = Finder.AchievementPublisher;
            soundPlayer = Finder.SoundPlayer;
        }

        private void Update()
        {
            if (brushing && !toothbrush.IsInteractedWith)
            {
                StopAllCoroutines();
                soundPlayer.StopSound(brushingNoise);
                ResetToothbrush();
            }
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player) && toothbrush.IsInteractedWith)
            {
                UnlockAchievementAfterTime();
            }
        }
        
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                soundPlayer.StopSound(brushingNoise);
                ResetToothbrush();
            }
        }

        private void ResetToothbrush()
        {
            toothbrush.EndInteraction();
            toothbrush.transform.position = startPosition;
            brushing = false;
        }
        
        private void UnlockAchievementAfterTime()
        {
            IEnumerator BrushingRoutine()
            {
                soundPlayer.PlaySound(brushingNoise, 0.25f);
                yield return new WaitForSeconds(brushTime);
                achievementPublisher.UnlockAchievement(AchievementType.Level1TeethBrushed);
                soundPlayer.StopSound(brushingNoise);
                ResetToothbrush();
                teethBrushed = true;
            }
            
            if (!brushing && !teethBrushed)
            {
                brushing = true;
                StartCoroutine(BrushingRoutine());
            }
        }
    }
}