﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class CoffeeZone : MonoBehaviour
    {
        [SerializeField] private Dragable coffeePlatter;
        [SerializeField] private Transform endZone;
        [Header("Sound")] 
        [SerializeField] private AudioClip brewingNoise;
        
        private AchievementPublisher achievementPublisher;
        private SoundPlayer soundPlayer;
        
        private Vector2 endPosition;

        private void Awake()
        {
            endPosition = endZone.position;
            achievementPublisher = Finder.AchievementPublisher;
            soundPlayer = Finder.SoundPlayer;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject == coffeePlatter.gameObject && coffeePlatter.IsInteractedWith)
            {
                achievementPublisher.UnlockAchievement(AchievementType.CoffeeTaken);
                soundPlayer.PlaySound(brewingNoise);
                ResetPlatter();
            }
        }
        
        private void ResetPlatter()
        {
            coffeePlatter.EndInteraction();
            coffeePlatter.transform.position = endPosition;
        }
    }
}