﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class FeedZone : MonoBehaviour
    {
        [SerializeField] private Dragable dogFood;
        [SerializeField] private Transform endZone;
        [SerializeField] private SpriteRenderer foodBowl;
        [SerializeField] private Sprite fullBowl;
        [Header("Sound")] 
        [SerializeField] private AudioClip foodNoise;
        [SerializeField] private AudioClip dogBark;
        
        private AchievementPublisher achievementPublisher;
        private SoundPlayer soundPlayer;
        
        // Empêche l'accomplissement de se débloquer une seconde fois
        private bool dogFed;
        private Vector2 endPosition;

        private void Awake()
        {
            endPosition = endZone.position;
            achievementPublisher = Finder.AchievementPublisher;
            soundPlayer = Finder.SoundPlayer;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject == dogFood.gameObject && dogFood.IsInteractedWith && !dogFed)
            {
                achievementPublisher.UnlockAchievement(AchievementType.Level1DogFed);
                foodBowl.sprite = fullBowl;
                soundPlayer.PlaySound(foodNoise);
                soundPlayer.PlaySound(dogBark, 0.25f);
                ResetFoodBag();
                dogFed = true;
            }
        }
        
        private void ResetFoodBag()
        {
            dogFood.EndInteraction();
            dogFood.transform.position = endPosition;
        }
    }
}