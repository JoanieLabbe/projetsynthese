﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class Cards : MonoBehaviour
    {
        [SerializeField] private AudioClip soundToPlay;

        private WalletEventChannel walletEventChannel;
        private SoundPlayer soundPlayer;

        private void Awake()
        {
            soundPlayer = Finder.SoundPlayer;
            
            walletEventChannel = Finder.WalletEventChannel;
            walletEventChannel.OnWalletLost += OnWalletLost;
        }

        private void OnDestroy()
        {
            walletEventChannel.OnWalletLost -= OnWalletLost;
        }

        private void OnWalletLost(Vector2 position)
        { 
            transform.position = position;
            soundPlayer.PlaySound(soundToPlay);
        }
    }
}