﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class Coworker : MonoBehaviour
    {
        [SerializeField] private GameObject coworker;
        
        private WalletEventChannel walletEventChannel;
        private bool eventIsDone;
        
        private void Awake()
        {
            walletEventChannel = Finder.WalletEventChannel;
            walletEventChannel.OnElevatorExit += OnElevatorExit;
        }
        
        private void OnDestroy()
        {
            walletEventChannel.OnElevatorExit -= OnElevatorExit;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player) && !eventIsDone)
            {
                walletEventChannel.ChangePosition(transform.position);
                eventIsDone = true;
            }
        }
        
        private void OnElevatorExit()
        {
            coworker.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}