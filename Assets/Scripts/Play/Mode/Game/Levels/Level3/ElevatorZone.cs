﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class ElevatorZone : MonoBehaviour
    {
        [SerializeField] private AudioClip soundToPlay;

        private WalletEventChannel walletEventChannel;
        private SoundPlayer soundPlayer;
        private bool eventIsDone;

        private void Awake()
        {
            soundPlayer = Finder.SoundPlayer;
            walletEventChannel = Finder.WalletEventChannel;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player) && !eventIsDone)
            {
                soundPlayer.PlaySound(soundToPlay);
                walletEventChannel.ShowCoworker();
                eventIsDone = true;
            }
        }
    }
}