﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.LevelController)]
    public class PutItemIntoInventoryAtStart : MonoBehaviour
    {
        [SerializeField] private List<InventoryItem> itemsToAdd;

        private void Start()
        {
            itemsToAdd.ForEach(item => Finder.Inventory.IncreaseQuantity(item));
        }
    }
}