﻿using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class DialogueModel : MonoBehaviour
    {
        [SerializeField] private LanguageTemplate frenchData;
        [SerializeField] private LanguageTemplate englishData;
        [SerializeField] private LanguageTemplate norwegianData;

        private LanguageTemplate language;
        public string[] Sentences { get; set; }
        public string ButtonText { get; set; }

        public void SetEnglish()
        {
            Sentences = englishData.level4Dialogue;
            ButtonText = englishData.continueButton;
        }

        public void SetFrench()
        {
            Sentences = frenchData.level4Dialogue;
            ButtonText = frenchData.continueButton;
        }

        public void SetNorwegian()
        {
            Sentences = norwegianData.level4Dialogue;
            ButtonText = norwegianData.continueButton;
        }

    }
}