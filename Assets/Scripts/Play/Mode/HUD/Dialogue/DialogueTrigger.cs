﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    public class DialogueTrigger : MonoBehaviour, IInteractable
    {
        private bool alreadyInteractedWith;
        
        private void Awake()
        {
            alreadyInteractedWith = false;
        }

        public void StartInteraction(Player sender)
        {
            Finder.DialogueEventChannel.Publish();
            if (!alreadyInteractedWith)
            {
                Finder.ObjectivesEventChannel.PublishCompleted();
                alreadyInteractedWith = true;
            }
        }

        public void EndInteraction()
        {
            // Rien à faire
        }
    }
}