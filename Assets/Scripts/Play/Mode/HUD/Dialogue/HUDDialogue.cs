﻿using System.Collections.Generic;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class HUDDialogue : MonoBehaviour
    {
        [SerializeField] private float cooldownTime;
        private const string ANIMATOR_PARAMETER = "IsOpen";
        
        private float waitingTime;
        
        private FlagUpdates flag;
        private OptionsController optionsController;
        private Languages language;

        private Queue<string> sentences;
        private DialogueModel model;
        private DialogueEventChannel eventChannel;
        
        private Animator animator;
        private TextMeshProUGUI dialogueText;
        private Button continueButton;

        private void Awake()
        {
            flag = Finder.FlagUpdates;
            optionsController = Finder.OptionsController;
            
            sentences = new Queue<string>();
            language = optionsController.Values.language;
            model = GetComponent<DialogueModel>();
            ChangeDialogueLanguage();

            FindComponents();

            eventChannel = Finder.DialogueEventChannel;
            eventChannel.OnDialogueStarted += EnqueueDialogue;
            waitingTime = Time.time;
        }

        private void OnEnable()
        {
            continueButton.onClick.AddListener(DisplayNextSentence);
        }

        private void OnDisable()
        {
            continueButton.onClick.RemoveListener(DisplayNextSentence);
        }

        private void OnDestroy()
        {
            eventChannel.OnDialogueStarted -= EnqueueDialogue;
        }

        private void EnqueueDialogue()
        {
            if (waitingTime <= Time.time)
            {
                continueButton.Select();
                animator.SetBool(ANIMATOR_PARAMETER, true);
                sentences.Clear();
                flag.PlayerShouldUpdate = false;
                flag.DialoguePersonShouldUpdate = false;
            
                VerifyLanguage();
            

                foreach (var s in model.Sentences)
                {
                    sentences.Enqueue(s);
                }
            
                DisplayNextSentence();
            }
        }

        private void DisplayNextSentence()
        {
            if (sentences.Count > 0)
            {
                dialogueText.text = sentences.Dequeue();
            }
            else
            {
                EndDialogue();
            }
        }
        
        private void EndDialogue()
        {
            animator.SetBool(ANIMATOR_PARAMETER, false);
            waitingTime = Time.time + cooldownTime;
            flag.PlayerShouldUpdate = true;
            flag.DialoguePersonShouldUpdate = true;
        }
        
        private void FindComponents()
        {
            var textFields = GetComponentsInChildren<TextMeshProUGUI>();
            
            dialogueText = textFields.WithName(GameObjects.DialogueText);
            continueButton = GetComponentInChildren<Button>();
            continueButton.GetComponentInChildren<TextMeshProUGUI>().text = model.ButtonText;
            
            animator = GetComponent<Animator>();
        }

        private void VerifyLanguage()
        {
            if (language != optionsController.Values.language)
            {
                ChangeDialogueLanguage();
                ChangeButtonLanguage();
            }
        }

        private void ChangeDialogueLanguage()
        {
            language = optionsController.Values.language;
            switch (language)
            {
                case Languages.FR:
                    model.SetFrench();
                    break;
                case Languages.ENG:
                    model.SetEnglish();
                    break;
                case Languages.NOR:
                    model.SetNorwegian();
                    break;
            }
        }

        private void ChangeButtonLanguage()
        {
            var buttonText = continueButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = model.ButtonText;
        }
        
    }
}