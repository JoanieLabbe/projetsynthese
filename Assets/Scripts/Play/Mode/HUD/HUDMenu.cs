﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class HUDMenu : MonoBehaviour
    {
        private HUDObjective objective;
        private HUDTimerController timerController;
        private LevelLoadedEventChannel levelLoadedEventChannel;
  
        private void Awake()
        {
            objective = Finder.HUDObjective;
            timerController = Finder.HUDTimerController;

            levelLoadedEventChannel = Finder.LevelLoadedEventChannel;
            levelLoadedEventChannel.OnLevelLoaded += OnLevelLoaded;
        }
        
        private void OnDestroy()
        {
            levelLoadedEventChannel.OnLevelLoaded -= OnLevelLoaded;
        }
        
        private void OnLevelLoaded(int level)
        {
            objective.UpdateObjectiveAtEndOfLevel(level);
            timerController.UpdateState(level);
        }
    }
}