﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    public class HUDInventory : MonoBehaviour
    {
        [SerializeField] private GameObject slot;

        private Inventory inventory;
        private HUDItemSlot itemSlot;
        private InventoryEventChannel inventoryEventChannel;
        
        private void Awake()
        {
            itemSlot = gameObject.AddComponent<HUDItemSlot>();
            
            inventoryEventChannel = Finder.InventoryEventChannel;
            
            inventoryEventChannel.OnItemAcquired += OnItemAcquired;
            inventoryEventChannel.OnItemRemoved += OnItemRemoved;
        }

        private void OnDestroy()
        {
            inventoryEventChannel.OnItemAcquired -= OnItemAcquired;
        }
        
        private void OnItemAcquired(InventoryItem inventoryItem)
        {
            // Note : On sait qu'on utilise un Finder dans une méthode. On va corriger en gold, puisque le mettre dans 
            // Awake impliquerais un déplacement de inventory, qui se trouve dans les levelcontroller (fait en sorte que le Finder est null en awake)
            if (Finder.Inventory.GetQuantity(inventoryItem) == 1)
            {
                itemSlot.CreateNewVisualItem(inventoryItem, slot);
            }
        }
        
        private void OnItemRemoved(InventoryItem inventoryItem)
        {
            itemSlot.DestroyParent(inventoryItem);
        }
    }
}