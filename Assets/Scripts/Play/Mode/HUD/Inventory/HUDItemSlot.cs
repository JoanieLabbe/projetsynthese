﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Carol-Ann Collin
    public class HUDItemSlot : MonoBehaviour
    {
        private const string IMAGE = "Image";
        private List<GameObject> itemsInInventory;

        private void Awake()
        {
            itemsInInventory = new List<GameObject>();
        }

        public void CreateNewVisualItem(InventoryItem inventoryItem, GameObject slot)
        {
            GameObject parent = Instantiate(slot, gameObject.transform, true);
            parent.name = inventoryItem.Tag;
            itemsInInventory.Add(parent);
            
            SetUpImage(inventoryItem, parent);
        }

        private void SetUpImage(InventoryItem inventoryItem, GameObject parent)
        {
            Image image = parent.transform.GetChild(0).GetComponentInChildren<Image>();
            image.sprite = inventoryItem.Sprite;
            image.name = IMAGE;
            image.transform.SetParent(parent.transform);
        }

        public void DestroyParent(InventoryItem inventoryItem)
        {
            foreach (GameObject item in itemsInInventory)
            {
                if (item != null && item.name == inventoryItem.Tag)
                {
                    Destroy(item);
                    return;
                }
            }
        }
    }
}