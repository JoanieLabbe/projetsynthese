﻿using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    // Author: Carol-Ann Collin et Kamylle Thériault
    [Findable(Tags.HUD)]
    public class HUDObjective : MonoBehaviour
    {
        [SerializeField][Range(5, 15)] private float letterSpeed = 0.1f;

        private int currentLevel;
        private int currentIndex;
        private int lastIndex;
        
        private string objective;
        private string[] ObjectivesLevel1;
        private string[] ObjectivesLevel2;
        private string[] ObjectivesLevel3;
        private string[] ObjectivesLevel4;
        private string[] ObjectivesLevel5;
        private TMP_Text objText;

        private ObjectivesLanguageModel languageModel;
        private ObjectivesEventChannel eventChannel;
        private LanguageEventChannel languageEventChannel;

        private void Awake()
        {
            currentIndex = 0;
            lastIndex = 0;
            
            languageModel = new ObjectivesLanguageModel();
            SetActiveLanguage();
            SetLabels();

            objText = GameObject.FindWithTag(Tags.Objective).GetComponent<TMP_Text>();

            eventChannel = Finder.ObjectivesEventChannel;
            eventChannel.OnObjectiveCompleted += ChangeCurrentObjective;
            eventChannel.OnObjectiveReset += ResetObjective;

            languageEventChannel = Finder.LanguageEventChannel;
            languageEventChannel.OnLanguageChangeEvent += ChangeActiveLanguage;
        }

        private void OnDestroy()
        {
            eventChannel.OnObjectiveCompleted -= ChangeCurrentObjective;
            eventChannel.OnObjectiveReset -= ResetObjective;
            languageEventChannel.OnLanguageChangeEvent -= ChangeActiveLanguage;
        }

        public void UpdateObjectiveAtEndOfLevel(int levelToLoad)
        {
            currentIndex = 0;
            currentLevel = levelToLoad;
            FindObjectiveToLoad();
            StartCoroutine(TypeObjective(objective));
        }

        private IEnumerator TypeObjective(string objectiveToType)
        {
            objText.text = "";
            foreach (char letter in objectiveToType)
            {
                if (objectiveToType != objective)
                {
                    break;
                }
                string temp = objText.text + letter;
                if (objective.StartsWith(temp))
                {
                    objText.text += letter;
                    if (letter != ' ')
                    {
                        yield return new WaitForSeconds(letterSpeed * Time.deltaTime);
                    }
                }
            }
        }

        private void ChangeCurrentObjective()
        {
            lastIndex = currentIndex;
            currentIndex++;
            FindObjectiveToLoad();
            StartCoroutine(TypeObjective(objective));
        }

        private void FindObjectiveToLoad()
        {
            objective = GetObjective(currentLevel, currentIndex);
        }

        private string GetObjective(int level, int index)
        {
            string objectiveToReturn = "";
            switch (level)
            {
                case 1:
                    if (ObjectivesLevel1.Length > index)
                    {
                        objectiveToReturn = ObjectivesLevel1[index];
                    }
                    break;
                case 2:
                    if (ObjectivesLevel2.Length > index)
                    {
                        objectiveToReturn = ObjectivesLevel2[index];
                    }
                    break;
                case 3:
                    if (ObjectivesLevel3.Length > index)
                    {
                        objectiveToReturn = ObjectivesLevel3[index];
                    }
                    break;
                case 4:
                    if (ObjectivesLevel4.Length > index)
                    {
                        objectiveToReturn = ObjectivesLevel4[index];
                    }
                    break;
                case 5:
                    if (ObjectivesLevel5.Length > index)
                    {
                        objectiveToReturn = ObjectivesLevel5[index];
                    }
                    break;
            }

            return objectiveToReturn;
        }
        
        private void ResetObjective()
        {
            currentIndex = lastIndex;
            FindObjectiveToLoad();
            StartCoroutine(TypeObjective(objective));
        }

        private void SetLabels()
        {
            ObjectivesLevel1 = languageModel.ObjectivesFirstLevel;
            ObjectivesLevel2 = languageModel.ObjectivesSecondLevel;
            ObjectivesLevel3 = languageModel.ObjectivesThirdLevel;
            ObjectivesLevel4 = languageModel.ObjectivesFourthLevel;
            ObjectivesLevel5 = languageModel.ObjectivesFifthLevel;
        }

        // Author : Vivianne Lord
        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }

        // Author : Vivianne Lord
        private void ChangeActiveLanguage()
        {
            SetActiveLanguage();
            SetLabels();
            FindObjectiveToLoad();
            StartCoroutine(TypeObjective(objective));
        }
    }
}