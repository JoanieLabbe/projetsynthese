﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class ObjectivesLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] ObjectivesFirstLevel { get; private set; }
        public string[] ObjectivesSecondLevel { get; private set; }
        public string[] ObjectivesThirdLevel { get; private set; }
        public string[] ObjectivesFourthLevel { get; private set; }
        public string[] ObjectivesFifthLevel { get; private set; }

        public ObjectivesLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            ObjectivesFirstLevel = languageAssets.FrenchAsset.objectiveLevel1Labels;
            ObjectivesSecondLevel = languageAssets.FrenchAsset.objectiveLevel2Labels;
            ObjectivesThirdLevel = languageAssets.FrenchAsset.objectiveLevel3Labels;
            ObjectivesFourthLevel = languageAssets.FrenchAsset.objectiveLevel4Labels;
            ObjectivesFifthLevel = languageAssets.FrenchAsset.objectiveLevel5Labels;
        }

        public void SetEnglish()
        {
            ObjectivesFirstLevel = languageAssets.EnglishAsset.objectiveLevel1Labels;
            ObjectivesSecondLevel = languageAssets.EnglishAsset.objectiveLevel2Labels;
            ObjectivesThirdLevel = languageAssets.EnglishAsset.objectiveLevel3Labels;
            ObjectivesFourthLevel = languageAssets.EnglishAsset.objectiveLevel4Labels;
            ObjectivesFifthLevel = languageAssets.EnglishAsset.objectiveLevel5Labels;
        }

        public void SetNorwegian()
        {
            ObjectivesFirstLevel = languageAssets.NorwegianAsset.objectiveLevel1Labels;
            ObjectivesSecondLevel = languageAssets.NorwegianAsset.objectiveLevel2Labels;
            ObjectivesThirdLevel = languageAssets.NorwegianAsset.objectiveLevel3Labels;
            ObjectivesFourthLevel = languageAssets.NorwegianAsset.objectiveLevel4Labels;
            ObjectivesFifthLevel = languageAssets.NorwegianAsset.objectiveLevel5Labels;
        }
    }
}