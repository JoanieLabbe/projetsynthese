﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Carol-Ann Collin
    public class HUDPopUpAchievement : MonoBehaviour
    {
        [SerializeField] private GameObject image;
        [SerializeField] private List<PopUpAchievement> possibleAchievements;
        [SerializeField][Range(0, 5)] private float delay;

        private Main main;
        private AchievementEventChannel achievementEventChannel;
        
        private void Start()
        {
            main = Finder.Main;
        }

        private void Update()
        {
            if (achievementEventChannel == null)
            {
                achievementEventChannel = Finder.AchievementEventChannel;
            }
            else
            {
                achievementEventChannel.OnAchievementUnlocked += OnAchievementUnlocked;
            }
        }

        private void OnAchievementUnlocked(AchievementType achievementType, DateTime datetime)
        {
            foreach (var popUpAchievement in possibleAchievements)
            {
                if (popUpAchievement.AchievementType == achievementType && main.LevelToLoad != -1)
                {
                    switch (achievementType)
                    {
                        case AchievementType.LevelCompleted:
                            StartCoroutine(ShowPopUp(popUpAchievement.Sprites.ElementAt(main.LevelToLoad - 1)));
                            break;
                        
                        default:
                            StartCoroutine(ShowPopUp(popUpAchievement.Sprites.First()));
                            break;
                    }
                }
            }
        }

        private IEnumerator ShowPopUp(Sprite sprite)
        {
            image.GetComponent<Image>().sprite = sprite;
            image.SetActive(true);
            yield return new WaitForSeconds(delay);
            image.SetActive(false);
        }
    }
}