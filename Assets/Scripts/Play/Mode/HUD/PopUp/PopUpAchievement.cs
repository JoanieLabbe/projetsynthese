﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    // Author: Carol-Ann Collin
    [CreateAssetMenu(fileName = "New Pop Up Achievement", menuName = "Game/Pop Up Achievement")]
    public class PopUpAchievement : ScriptableObject
    {
        [SerializeField] private AchievementType achievementType;
        [SerializeField] private List<Sprite> sprites;

        public AchievementType AchievementType => achievementType;
        public List<Sprite> Sprites => sprites;
    }
}