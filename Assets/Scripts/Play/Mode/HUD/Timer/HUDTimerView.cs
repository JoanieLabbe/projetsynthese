﻿using TMPro;

namespace Game
{
    // Author : Carol-Ann Collin
    public class HUDTimerView
    {
        public TMP_Text TimerText { get; set; }

        public void UpdateTimerText(string formattedTime)
        {
           TimerText.text = formattedTime;
        }

        public void UpdateState(bool state)
        {
            TimerText.enabled = state;
        }
    }
}