﻿using TMPro;
using UnityEngine.UI;

namespace Game
{
    // Author : Vivianne Lord
    public class HUDTutorial
    {
        public TextMeshProUGUI TutorialText { get; set; }
        public Image TutorialBackground { get; set; }

        public void ShowPrompt(string nextPrompt)
        {
            TutorialText.SetText(nextPrompt);
            TutorialBackground.enabled = true;
            TutorialText.enabled = true;
        }

        public void MaskPrompt()
        {
            TutorialBackground.enabled = false;
            TutorialText.enabled = false;
        }
    }
}