﻿using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    // Author : Vivianne Lord
    public class TutorialController : MonoBehaviour
    {
        private HUDTutorial view;
        private TutorialModel model;
        private TutorialEventChannel tutorialEventChannel;
        private LanguageEventChannel languageEventChannel;

        private bool playerInZone;
        private string currentZone;

        private void Awake()
        {
            view = new HUDTutorial();
            model = new TutorialModel();
            SetTutorialLanguage();

            view.TutorialText = GetComponentInChildren<TextMeshProUGUI>();
            view.TutorialBackground = GetComponentInChildren<Image>();

            tutorialEventChannel = Finder.TutorialEventChannel;
            tutorialEventChannel.OnPromptDisplay += ShowTutorialPrompt;
            tutorialEventChannel.OnPromptMasking += MaskTutorialPrompt;

            languageEventChannel = Finder.LanguageEventChannel;
            languageEventChannel.OnLanguageChangeEvent += ChangeActiveLanguage;
            
            MaskTutorialPrompt();
        }
        
        private void SetTutorialLanguage()
        {
            switch (model.ActiveLanguage)
            {
                case Languages.FR:
                    model.SetFrench();
                    break;
                case Languages.ENG:
                    model.SetEnglish();
                    break;
                case Languages.NOR:
                    model.SetNorwegian();
                    break;
            }
        }

        private void ChangeActiveLanguage()
        {
            SetTutorialLanguage();
            if (playerInZone)
            {
                view.ShowPrompt(NextTutorialPrompt(currentZone));
            }
        }

        private void ShowTutorialPrompt(string tutorialZone)
        {
            playerInZone = true;
            currentZone = tutorialZone;
            string prompt = NextTutorialPrompt(tutorialZone);
            view.ShowPrompt(prompt);
        }

        private void MaskTutorialPrompt()
        {
            playerInZone = false;
            view.MaskPrompt();
        }

        private string NextTutorialPrompt(string tutorialZone)
        {
            switch (tutorialZone)
            {
                case GameObjects.TutorialFirstPromptZone:
                    return model.Labels[0];
                case GameObjects.TutorialSecondPromptZone:
                    return model.Labels[1];
                case GameObjects.TutorialThirdPromptZone:
                    return model.Labels[2];
                case GameObjects.TutorialFourthPromptZone:
                    return model.Labels[3];
                case GameObjects.TutorialFifthPromptZone:
                    return model.Labels[4];
                default:
                    return null;
            }
        }

        private void OnDestroy()
        {
            tutorialEventChannel.OnPromptDisplay -= ShowTutorialPrompt;
            tutorialEventChannel.OnPromptMasking -= MaskTutorialPrompt;
            languageEventChannel.OnLanguageChangeEvent -= ChangeActiveLanguage;
        }
    }
}