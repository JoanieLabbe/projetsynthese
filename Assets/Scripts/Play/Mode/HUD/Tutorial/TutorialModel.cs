﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class TutorialModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] Labels { get; private set; }

        public TutorialModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }

        public void SetFrench()
        {
            Labels = languageAssets.FrenchAsset.tutorialLabels;
        }
        
        public void SetEnglish()
        {
            Labels = languageAssets.EnglishAsset.tutorialLabels;
        }

        public void SetNorwegian()
        {
            Labels = languageAssets.NorwegianAsset.tutorialLabels;
        }
    }
}