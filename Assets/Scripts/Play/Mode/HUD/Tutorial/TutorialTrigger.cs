﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    public class TutorialTrigger : MonoBehaviour
    {
        private bool hasAppeared;
        
        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player) && !hasAppeared)
            {
                Finder.TutorialEventChannel.PublishPrompt(name);
                hasAppeared = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag(Tags.Player))
            {
                Finder.TutorialEventChannel.PublishMasking();
            }
        }
    }
}