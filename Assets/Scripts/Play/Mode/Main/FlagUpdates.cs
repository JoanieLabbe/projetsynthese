﻿using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Kamylle Thériault
    [Findable(Tags.MainController)]
    public class FlagUpdates : MonoBehaviour
    {
        public bool PlayerShouldUpdate { get; set; }
        public bool EnemyShouldUpdate { get; set; }
        public bool DialoguePersonShouldUpdate { get; set; }
        public bool PauseCanvasShouldUpdate { get; set; }
        public bool SpawnersShouldUpdate { get; set; }
        public bool InteractibleDogShouldUpdate { get; set; }

        private void Awake()
        {
            SpawnersShouldUpdate = true;
            PlayerShouldUpdate = true;
            EnemyShouldUpdate = true;
            PauseCanvasShouldUpdate = true;
            InteractibleDogShouldUpdate = true;
            DialoguePersonShouldUpdate = true;
        }
    }
}