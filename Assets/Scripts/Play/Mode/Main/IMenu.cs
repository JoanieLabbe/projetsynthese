﻿namespace Game
{
    // Author : Kamylle Thériault
    public interface IMenu
    {
        void Show();
        void Hide();
    }
}