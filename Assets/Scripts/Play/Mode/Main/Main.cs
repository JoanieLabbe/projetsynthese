using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    [Findable(Tags.MainController)]
    public class Main : MonoBehaviour
    {
        [Header("Menu Scenes")]
        [SerializeField] private SceneBundle homeScenes;
        [SerializeField] private SceneBundle gameScenes;
        [SerializeField] private SceneBundle optionsScenes;
        [SerializeField] private SceneBundle fileSelectionScenes;
        [SerializeField] private SceneBundle levelSelectionScenes;
        [SerializeField] private SceneBundle achievementsMenuScenes;

        [Header("Level Scenes")] 
        [SerializeField] private SceneBundle level1Scenes;
        [SerializeField] private SceneBundle level2Scenes;
        [SerializeField] private SceneBundle level3Scenes;
        [SerializeField] private SceneBundle level4Scenes;
        [SerializeField] private SceneBundle level5Scenes;

        private LevelCompleteEventChannel levelCompleteEventChannel;
        private LevelLoadedEventChannel levelLoadedEventChannel;
        private LevelFailedEventChannel levelFailedEventChannel;

        private SceneBundleLoader loader;
        private AudioSource menuMusic;

        public int LevelToLoad { get; set; }

        private void Awake()
        {
            loader = Finder.SceneBundleLoader;

            levelCompleteEventChannel = Finder.LevelCompleteEventChannel;
            levelCompleteEventChannel.OnLevelComplete += OnLevelComplete;
            
            levelLoadedEventChannel = Finder.LevelLoadedEventChannel;
            
            levelFailedEventChannel = Finder.LevelFailedEventChannel;
            levelFailedEventChannel.OnLevelFailed += OnLevelFailed;

            menuMusic = GetComponentInChildren<AudioSource>();
            menuMusic.Play();

            DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
            DOTween.SetTweensCapacity(200, 125);
        }

        private IEnumerator Start()
        {
#if UNITY_EDITOR
            if (gameScenes.IsLoaded)
                yield return loader.Load(gameScenes);
            else
#endif
                yield return loader.Load(fileSelectionScenes);
        }
        
        // Author : Marc-Antoine Sigouin
        public void LoadLevel()
        {
            switch (LevelToLoad)
            {
                case 1:
                    loader.Load(level1Scenes);
                    break;
                case 2:
                    loader.Load(level2Scenes);
                    break;
                case 3:
                    loader.Load(level3Scenes);
                    break;
                case 4:
                    loader.Load(level4Scenes);
                    break;
                case 5:
                    loader.Load(level5Scenes);
                    break;
                default:
                    EndGame();
                    break;
            }

            levelLoadedEventChannel.Publish(LevelToLoad);
        }

        // Author : Marc-Antoine Sigouin
        public void ReloadLevel()
        {
            switch (LevelToLoad)
            {
                case 1:
                    loader.Reload(level1Scenes);
                    break;

                case 2:
                    loader.Reload(level2Scenes);
                    break;
                case 3:
                    loader.Reload(level3Scenes);
                    break;
                case 4:
                    loader.Reload(level4Scenes);
                    break;
                case 5:
                    loader.Reload(level5Scenes);
                    break;
                default:
                    EndGame();
                    break;
            }
        }

        // Author : Marc-Antoine Sigouin
        public void UnloadCurrentLoadedLevel()
        {
            if (level1Scenes.IsLoaded)
            {
                loader.Unload(level1Scenes);
                LevelToLoad = 2;
            }
            if (level2Scenes.IsLoaded)
            {
                loader.Unload(level2Scenes);
                LevelToLoad = 3;
            }
            if (level3Scenes.IsLoaded)
            {
                loader.Unload(level3Scenes);
                LevelToLoad = 4;
            }
            if (level4Scenes.IsLoaded)
            {
                loader.Unload(level4Scenes);
                LevelToLoad = 5;
            }
            if (level5Scenes.IsLoaded)
            {
                loader.Unload(level5Scenes);
                LevelToLoad = -1; // pour retourner au menu principal.
            }
        }

        // Author : Marc-Antoine Sigouin
        public void EndGame()
        {
            LoadHomeScenes();
            UnloadGameScenes();
        }

        public Coroutine UnloadFileSelectionScenes()
        {
            return loader.Unload(fileSelectionScenes);
        }
        
        public Coroutine LoadHomeScenes()
        {
            if (!menuMusic.isPlaying)
            {
                menuMusic.Play();
            }
            return loader.Load(homeScenes);
        }
        
        public Coroutine UnloadHomeScenes()
        {
            if (menuMusic.isPlaying)
            {
                menuMusic.Stop();
            }
            return loader.Unload(homeScenes);
        }

        public Coroutine LoadGameScenes()
        {
            StartCoroutine(LoadHUD());
            return loader.Load(gameScenes);
        }
        
        public Coroutine UnloadGameScenes()
        {
            StartCoroutine(UnloadHUD());
            return loader.Unload(gameScenes);
        }

        public Coroutine LoadOptionsScenes()
        {
            return loader.Load(optionsScenes);
        }
        
        public Coroutine UnloadOptionsScenes()
        {
            return loader.Unload(optionsScenes);
        }
        
        public Coroutine LoadAchievementsMenuScenes()
        {
            return loader.Load(achievementsMenuScenes);
        }
        
        public Coroutine UnloadAchievementsMenuScenes()
        {
            return loader.Unload(achievementsMenuScenes);
        }
        
        public Coroutine LoadLevelSelectionScenes()
        {
            return loader.Load(levelSelectionScenes);
        }
        
        public Coroutine UnloadLevelSelectionScenes()
        {
            return loader.Unload(levelSelectionScenes);
        }

        // Author : Marc-Antoine Sigouin
        private void OnLevelComplete()
        {
            UnloadCurrentLoadedLevel();
            LoadLevel();
        }

        // Author : Marc-Antoine Sigouin
        private void OnLevelFailed()
        {
            ReloadLevel();
        }
        
        // Author: Carol-Ann Collin
        private IEnumerator LoadHUD()
        {
            yield return SceneManager.LoadSceneAsync(Scenes.UI, LoadSceneMode.Additive);
        }
        
        // Author: Carol-Ann Collin
        private IEnumerator UnloadHUD()
        {
            yield return SceneManager.UnloadSceneAsync(Scenes.UI);
        }
    }
}