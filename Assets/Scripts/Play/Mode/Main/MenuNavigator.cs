﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Benjamin Lemelin et Kamylle Thériault
    [Findable(Tags.MainController)]
    public class MenuNavigator : MonoBehaviour
    {
        private Stack<IMenu> menuStack;

        private void Awake()
        {
            menuStack = new Stack<IMenu>();
        }

        public void Push(IMenu menu)
        {
            if (menuStack.Count > 0)
            {
                menuStack.Peek().Hide();
                menu.Show();
            }
            menuStack.Push(menu);
        }
        
        public void Pop()
        {
            var poppedMenu = menuStack.Pop();
            poppedMenu.Hide();
            if (menuStack.Count > 0)
            {
                menuStack.Peek().Show();
            }
        }

        public void InverseMenuOrder()
        {
            if (menuStack.Count == 2)
            {
                var firstPopped = menuStack.Pop();
                var secondPopped = menuStack.Pop();
                menuStack.Push(firstPopped);
                menuStack.Push(secondPopped);
            }

            Pop();
        }
        
    }
}