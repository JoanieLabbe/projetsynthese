﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class AchievementsLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string Title { get; private set; }
        public string[] Names { get; private set; }
        public string[] Descriptions { get; private set; }
        public string[] Status { get; private set; }
        public string Button { get; private set; }

        public AchievementsLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Title = languageAssets.FrenchAsset.Title;
            Names = languageAssets.FrenchAsset.achievementName;
            Descriptions = languageAssets.FrenchAsset.achievementDescription;
            Status = languageAssets.FrenchAsset.achievementStatus;
            Button = languageAssets.FrenchAsset.backButton;
        }

        public void SetEnglish()
        {
            Title = languageAssets.EnglishAsset.Title;
            Names = languageAssets.EnglishAsset.achievementName;
            Descriptions = languageAssets.EnglishAsset.achievementDescription;
            Status = languageAssets.EnglishAsset.achievementStatus;
            Button = languageAssets.EnglishAsset.backButton;
        }

        public void SetNorwegian()
        {
            Title = languageAssets.NorwegianAsset.Title;
            Names = languageAssets.NorwegianAsset.achievementName;
            Descriptions = languageAssets.NorwegianAsset.achievementDescription;
            Status = languageAssets.NorwegianAsset.achievementStatus;
            Button = languageAssets.NorwegianAsset.backButton;
        }
    }
}