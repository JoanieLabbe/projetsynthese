﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class AchievementsMenu : MonoBehaviour, IMenu
    {
        private const string ACHIEVEMENTS_CONTAINER_TAG = "AchievementButtons";
        private const int LOCKED_ACHIEVEMENT_INDEX = 0;
        private const string UNLOCKED_ACHIEVEMENT_EMPTY = " ";

        private const int BUTTON_AMOUNT = 15;

        private Main main;
        private MenuNavigator menuNavigator;
        private SaveFileSelection saveFile;
        private List<AchievementEntry> unlockedAchievements;
        private AchievementsLanguageModel languageModel;

        private Button[] achievementButtons;
        private Button backButton;
        private Image[] lockedAchievements;
        private Image scribbledDescription; 

        private TextMeshProUGUI menuTitle;
        private TextMeshProUGUI[] backButtonLabels;
        private TextMeshProUGUI cardTitleText;
        private TextMeshProUGUI cardDescriptionText;
        private TextMeshProUGUI cardStatusText;
        
        private string[] achievementTitles;
        private string[] achievementDescriptions;
        private string[] achievementStatus;

        private Canvas canvas;
        
        private void Awake()
        {
            main = Finder.Main;
            menuNavigator = Finder.MenuNavigator;
            unlockedAchievements = Finder.SaveFileSelection.Model.unlockedAchievements;
            languageModel = new AchievementsLanguageModel();
            SetActiveLanguage();
            
            achievementTitles = new string[languageModel.Names.Length];
            achievementDescriptions = new string[languageModel.Descriptions.Length];
            // Il y a deux types de status, mais chaque accomplissement possède un status, d'où le achievementName.Length
            achievementStatus = new string[languageModel.Names.Length];

            canvas = GetComponent<Canvas>();
            InitializeImages();

            FindButtonsInView();
            FindLabelsInView();
            SetInitialTextValues();
            SetDefaultInformation();
        }

        private void Start()
        {
            menuNavigator.Push(this);
        }

        private void OnEnable()
        {
            achievementButtons[0].onClick.AddListener(ShowFirstAchievement);
            achievementButtons[1].onClick.AddListener(ShowSecondAchievement);
            achievementButtons[2].onClick.AddListener(ShowThirdAchievement);
            achievementButtons[3].onClick.AddListener(ShowFourthAchievement);
            achievementButtons[4].onClick.AddListener(ShowFifthAchievement);
            achievementButtons[5].onClick.AddListener(ShowSixthAchievement);
            achievementButtons[6].onClick.AddListener(ShowSeventhAchievement);
            achievementButtons[7].onClick.AddListener(ShowEighthAchievement);
            achievementButtons[8].onClick.AddListener(ShowNinthAchievement);
            achievementButtons[9].onClick.AddListener(ShowTenthAchievement);
            achievementButtons[10].onClick.AddListener(ShowEleventhAchievement);
            achievementButtons[11].onClick.AddListener(ShowTwelfthAchievement);
            achievementButtons[12].onClick.AddListener(ShowThirteenthAchievement);
            achievementButtons[13].onClick.AddListener(ShowFourteenthAchievement);
            achievementButtons[14].onClick.AddListener(ShowFifteenthAchievement);
            
            backButton.onClick.AddListener(ReturnToMainMenu);
        }

        private void OnDisable()
        {
            achievementButtons[0].onClick.RemoveListener(ShowFirstAchievement);
            achievementButtons[1].onClick.RemoveListener(ShowSecondAchievement);
            achievementButtons[2].onClick.RemoveListener(ShowThirdAchievement);
            achievementButtons[3].onClick.RemoveListener(ShowFourthAchievement);
            achievementButtons[4].onClick.RemoveListener(ShowFifthAchievement);
            achievementButtons[5].onClick.RemoveListener(ShowSixthAchievement);
            achievementButtons[6].onClick.RemoveListener(ShowSeventhAchievement);
            achievementButtons[7].onClick.RemoveListener(ShowEighthAchievement);
            achievementButtons[8].onClick.RemoveListener(ShowNinthAchievement);
            achievementButtons[9].onClick.RemoveListener(ShowTenthAchievement);
            achievementButtons[10].onClick.RemoveListener(ShowEleventhAchievement);
            achievementButtons[11].onClick.RemoveListener(ShowTwelfthAchievement);
            achievementButtons[12].onClick.RemoveListener(ShowThirteenthAchievement);
            achievementButtons[13].onClick.RemoveListener(ShowFourteenthAchievement);
            achievementButtons[14].onClick.RemoveListener(ShowFifteenthAchievement);
            
            backButton.onClick.RemoveListener(ReturnToMainMenu);
        }

        private void Update()
        {
            SelectButtonIfNoneSelected();
        }
        
        private void FindButtonsInView()
        {
            backButton = GetComponentsInChildren<Button>().WithName(GameObjects.BackToMain);
            var buttons = GameObject.FindWithTag(ACHIEVEMENTS_CONTAINER_TAG);
            achievementButtons = buttons.GetComponentsInChildren<Button>();
        }
        
        private void FindLabelsInView()
        {
            var labels = GetComponentsInChildren<TextMeshProUGUI>();
            cardTitleText = labels.WithName(GameObjects.TitleAchievement);
            cardDescriptionText = labels.WithName(GameObjects.DescriptionAchievement);
            cardStatusText = labels.WithName(GameObjects.AchievementStatus);
            menuTitle = labels.WithName(GameObjects.Title);
            backButtonLabels = backButton.GetComponentsInChildren<TextMeshProUGUI>();
        }
        
        private void InitializeImages()
        {
            var images = GetComponentsInChildren<Image>();
            scribbledDescription = images.WithName(GameObjects.Scribble);
            int index = 0;
            lockedAchievements = new Image[BUTTON_AMOUNT];
            foreach (var image in images)
            {
                if (image.name == GameObjects.LockedImage)
                {
                    lockedAchievements[index] = image;
                    index++;
                }
            }
        }

        private void SetUnlockedAchievement()
        {
            foreach (var achievement in unlockedAchievements)
            {
                switch (achievement.achievementType)
                {
                    case AchievementType.LevelCompleted:
                        FindFinishedLevel(achievement);
                        break;
                    case AchievementType.Level1PutAllDogToysInBox:
                        SetStatus(5);
                        break;
                    case AchievementType.Level1TeethBrushed:
                        SetStatus(6);
                        break;
                    case AchievementType.Level1DogFed:
                        SetStatus(7);
                        break;
                    case AchievementType.Level2BallReturnedToPark:
                        SetStatus(8);
                        break;
                    case AchievementType.Level3CompletedOnTime:
                        SetStatus(9);
                        break;
                    case AchievementType.Level3CollectedAllCards:
                        SetStatus(10);
                        break;
                    case AchievementType.Level5HitByChildOnSwing:
                        SetStatus(11);
                        break;
                    case AchievementType.Level5KeysPickedUp:
                        SetStatus(12);
                        break;
                    case AchievementType.Level1And3TookAllCoffee:
                        SetStatus(13);
                        break;
                    case AchievementType.PlayerHitByVehicle:
                        SetStatus(14);
                        break;
                }
            }
        }

        private void FindFinishedLevel(AchievementEntry achievement)
        {
            switch (achievement.level)
            {
                case 1:
                    SetStatus(0);
                    break;
                case 2:
                    SetStatus(1);
                    break;
                case 3:
                    SetStatus(2);
                    break;
                case 4:
                    SetStatus(3);
                    break;
                case 5:
                    SetStatus(4);
                    break;
            }
        }

        private void SetInitialTextValues()
        {
            for (int index = 0; index < achievementTitles.Length; index++)
            {
                achievementTitles[index] = languageModel.Names[index];
            }
            for (int index = 0; index < achievementDescriptions.Length; index++)
            {
                achievementDescriptions[index] = languageModel.Descriptions[index];
            }
            for (int index = 0; index < achievementStatus.Length; index++)
            {
                achievementStatus[index] = languageModel.Status[LOCKED_ACHIEVEMENT_INDEX];
            }
            menuTitle.text = languageModel.Title;
            foreach (var t in backButtonLabels)
            {
                t.text = languageModel.Button;
            }
            SetUnlockedAchievement();
        }

        // Author : Vivianne Lord
        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }

        private void SetStatus(int index)
        {
            achievementStatus[index] = UNLOCKED_ACHIEVEMENT_EMPTY;
            lockedAchievements[index].enabled = false;
        }

        private void SetDefaultInformation()
        {
            SwitchInformation(0);
        }

        private void ShowFirstAchievement()
        {
            SwitchInformation(0);
        }
        
        private void ShowSecondAchievement()
        {
            SwitchInformation(1);
        }
        
        private void ShowThirdAchievement()
        {
            SwitchInformation(2);
        }
        
        private void ShowFourthAchievement()
        {
            SwitchInformation(3);
        }
        
        private void ShowFifthAchievement()
        {
            SwitchInformation(4);
        }
        
        private void ShowSixthAchievement()
        {
            SwitchInformation(5);
        }
        
        private void ShowSeventhAchievement()
        {
            SwitchInformation(6);
        }
        
        private void ShowEighthAchievement()
        {
            SwitchInformation(7);
        }

        private void ShowNinthAchievement()
        {
            SwitchInformation(8);
        }
        
        private void ShowTenthAchievement()
        {
            SwitchInformation(9);
        }
        
        private void ShowEleventhAchievement()
        {
            SwitchInformation(10);
        }
        
        private void ShowTwelfthAchievement()
        {
            SwitchInformation(11);
        }

        private void ShowThirteenthAchievement()
        {
            SwitchInformation(12);
        }
        
        private void ShowFourteenthAchievement()
        {
            SwitchInformation(13);
        }
        
        private void ShowFifteenthAchievement()
        {
            SwitchInformation(14);
        }

        private void SwitchInformation(int index)
        {
            cardTitleText.text = achievementTitles[index];
            cardDescriptionText.text = achievementDescriptions[index];
            cardStatusText.text = achievementStatus[index];
            if (achievementStatus[index] == UNLOCKED_ACHIEVEMENT_EMPTY)
            {
                scribbledDescription.enabled = false;
            }
            else
            {
                scribbledDescription.enabled = true;
            }
        }
        
        private void ReturnToMainMenu()
        {
            menuNavigator.Pop();
            IEnumerator Routine()
            {
                yield return main.UnloadAchievementsMenuScenes();
            }
            
            StartCoroutine(Routine());
        }
        
        
        private void SelectButtonIfNoneSelected()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                backButton.Select();
            }
        }

        public void Show()
        {
            canvas.enabled = true;
            backButton.Select();
        }

        public void Hide()
        {
            canvas.enabled = true;
        }
    }
}