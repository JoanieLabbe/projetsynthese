﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class FileSelectionLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] Labels { get; private set; }
        public string[] Titles { get; private set; }
        public string[] Buttons { get; private set; }
        public string[] Names { get; private set; }

        public FileSelectionLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Labels = languageAssets.FrenchAsset.fileSelectionLabels;
            Titles = languageAssets.FrenchAsset.fileTitles;
            Buttons = languageAssets.FrenchAsset.fileSelectionButtons;
            Names = languageAssets.FrenchAsset.levelNames;
        }

        public void SetEnglish()
        {
            Labels = languageAssets.EnglishAsset.fileSelectionLabels;
            Titles = languageAssets.EnglishAsset.fileTitles;
            Buttons = languageAssets.EnglishAsset.fileSelectionButtons;
            Names = languageAssets.EnglishAsset.levelNames;
        }

        public void SetNorwegian()
        {
            Labels = languageAssets.NorwegianAsset.fileSelectionLabels;
            Titles = languageAssets.NorwegianAsset.fileTitles;
            Buttons = languageAssets.NorwegianAsset.fileSelectionButtons;
            Names = languageAssets.NorwegianAsset.levelNames;
        }
    }
}