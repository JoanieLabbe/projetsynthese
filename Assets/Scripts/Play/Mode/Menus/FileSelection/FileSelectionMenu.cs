﻿using System;
using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class FileSelectionMenu : MonoBehaviour
    {
        [Header("Sprites")]
        [SerializeField] private Sprite imageLevel1;
        [SerializeField] private Sprite imageLevel2;
        [SerializeField] private Sprite imageLevel3;
        [SerializeField] private Sprite imageLevel4;
        [SerializeField] private Sprite imageLevel5;
        [SerializeField] private Sprite defaultSprite;

        private Main main;
        private InputActions.MenuActions menuInputs;
        private FileSelectionLanguageModel languageModel;
        private bool deleteMode;
        
        private string[] fileSelectionLabels;
        private string[] fileTitles;
        private string[] fileSelectionButtons;
        private string[] levelNames;

        private Button firstFileButton;
        private Button secondFileButton;
        private Button thirdFileButton;
        private Button deleteFileButton;
        private Image firstFileImage;
        private Image secondFileImage;
        private Image thirdFileImage;
        private TextMeshProUGUI firstFileTitle;
        private TextMeshProUGUI secondFileTitle;
        private TextMeshProUGUI thirdFileTitle;
        private TextMeshProUGUI firstFileDescription;
        private TextMeshProUGUI secondFileDescription;
        private TextMeshProUGUI thirdFileDescription;

        private TextMeshProUGUI menuTitle;

        private SaveFileSelection saveFileSelection;

        private void Awake()
        {
            main = Finder.Main;
            menuInputs = Finder.Inputs.Actions.Menu;
            deleteMode = false;
            saveFileSelection = Finder.SaveFileSelection;
            languageModel = new FileSelectionLanguageModel();
            
            DetermineLanguage();
            InitializeAllText();
            FindButtonsInView();
            FindLabelsInView();
            SetImageToButtons();
            SetLabelsToButtons();
        }

        private void Start()
        {
            menuInputs.Enable();
            firstFileButton.Select();
        }
        
        private void OnEnable()
        {
            firstFileButton.onClick.AddListener(SelectFirstFile);
            secondFileButton.onClick.AddListener(SelectSecondFile);
            thirdFileButton.onClick.AddListener(SelectThirdFile);
            deleteFileButton.onClick.AddListener(ChangeShouldDelete);
        }

        private void OnDisable()
        {
            firstFileButton.onClick.RemoveListener(SelectFirstFile);
            secondFileButton.onClick.RemoveListener(SelectSecondFile);
            thirdFileButton.onClick.RemoveListener(SelectThirdFile);
            deleteFileButton.onClick.RemoveListener(ChangeShouldDelete);
        }

        private void Update()
        {
            SelectButtonIfNoneSelected();
        }

        private void FindLabelsInView()
        {
            var labels = GetComponentsInChildren<TextMeshProUGUI>();
            menuTitle = labels.WithName(GameObjects.Title);
            menuTitle.text = fileSelectionLabels[0];
        }

        private void FindButtonsInView()
        {
            var buttons = GetComponentsInChildren<Button>();
            firstFileButton = buttons.WithName(GameObjects.File1);
            secondFileButton = buttons.WithName(GameObjects.File2);
            thirdFileButton = buttons.WithName(GameObjects.File3);
            deleteFileButton = buttons.WithName(GameObjects.DeleteSaveFile);

            TextMeshProUGUI[] deleteText = deleteFileButton.GetComponentsInChildren<TextMeshProUGUI>();
            for (int g = 0; g < deleteText.Length; g++)
            {
                deleteText[g].text = fileSelectionButtons[0];
            }

            firstFileImage = firstFileButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageFile1);
            secondFileImage = secondFileButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageFile2);
            thirdFileImage = thirdFileButton.GetComponentsInChildren<Image>().WithName(GameObjects.ImageFile3);
            
            firstFileTitle = firstFileButton.GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.TitleSafeFile1);
            secondFileTitle = secondFileButton.GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.TitleSafeFile2);
            thirdFileTitle = thirdFileButton.GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.TitleSafeFile3);
            
            firstFileDescription = firstFileButton.GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.DescriptionSafeFile1);
            secondFileDescription = secondFileButton.GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.DescriptionSafeFile2);
            thirdFileDescription = thirdFileButton.GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.DescriptionSafeFile3);
        }

        private void SelectFirstFile()
        {
            SelectFile(1);
        }

        private void SelectSecondFile()
        {
            SelectFile(2);
        }

        private void SelectThirdFile()
        {
            SelectFile(3);
        }

        private void SelectButtonIfNoneSelected()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                deleteFileButton.Select();
            }
        }

        // Author : Joanie Labbe
        private void SelectFile(int saveFile)
        {
            if (!deleteMode)
            {
                saveFileSelection.ChooseSaveFile(saveFile);
                LeadToHomeScene();
            }
            else
            {
                saveFileSelection.DeleteSaveFile(saveFile);
                SetImageToButtons();
                SetLabelsToButtons();
                ChangeShouldDelete();
            }
        }

        private void ChangeShouldDelete()
        {
            if (!deleteMode)
            {
                deleteMode = true;
            }
            else
            {
                deleteMode = false;
            }

            ChangeTitle();
        }

        private void LeadToHomeScene()
        {
            IEnumerator Routine()
            {
                yield return main.LoadHomeScenes();
                yield return main.UnloadFileSelectionScenes();
            }
            
            StartCoroutine(Routine());
        }

        private void ChangeTitle()
        {
            if (!deleteMode)
            {
                menuTitle.text = fileSelectionLabels[0];
            }
            else
            {
                menuTitle.text = fileSelectionLabels[1];
            }
        }

        // Author : Joanie Labbe
        private Sprite GetImageForLevel(int level)
        {
            Sprite spriteToDisplay;
            switch (level)
            {
                case 1:
                    spriteToDisplay = imageLevel1;
                    break;
                case 2:
                    spriteToDisplay = imageLevel2;
                    break;
                case 3:
                    spriteToDisplay = imageLevel3;
                    break;
                case 4:
                    spriteToDisplay = imageLevel4;
                    break;
                case 5:
                    spriteToDisplay = imageLevel5;
                    break;
                default:
                    spriteToDisplay = defaultSprite;
                    break;
            }

            return spriteToDisplay;
        }
        
        private bool GetIfFileIsEmpty(int level)
        {
            return level == 0;
        }
        
        private string GetDescription(int level)
        {
            string description;
            switch (level)
            {
                case 1:
                    description = levelNames[0];
                    break;
                case 2:
                    description = levelNames[1];
                    break;
                case 3:
                    description = levelNames[2];
                    break;
                case 4:
                    description = levelNames[3];
                    break;
                case 5:
                    description = levelNames[4];
                    break;
                default:
                    description = "";
                    break;
            }

            return description;
        }

        // Author : Joanie Labbe
        private void SetImageToButtons()
        {
            firstFileImage.sprite = GetImageForLevel(saveFileSelection.GetLastLevelForSaveFile(1));
            secondFileImage.sprite = GetImageForLevel(saveFileSelection.GetLastLevelForSaveFile(2));
            thirdFileImage.sprite = GetImageForLevel(saveFileSelection.GetLastLevelForSaveFile(3));
        }

        private void SetLabelsToButtons()
        {
            firstFileDescription.text = GetDescription(saveFileSelection.GetLastLevelForSaveFile(1));
            secondFileDescription.text = GetDescription(saveFileSelection.GetLastLevelForSaveFile(2));
            thirdFileDescription.text = GetDescription(saveFileSelection.GetLastLevelForSaveFile(3));

            var fileIsEmpty = GetIfFileIsEmpty(saveFileSelection.GetLastLevelForSaveFile(1));
            if (!fileIsEmpty)
            {
                firstFileTitle.text = fileTitles[0];
            }
            else
            {
                firstFileTitle.text = fileTitles[3];
            }
            fileIsEmpty = GetIfFileIsEmpty(saveFileSelection.GetLastLevelForSaveFile(2));
            if (!fileIsEmpty)
            {
                secondFileTitle.text = fileTitles[1];
            }
            else
            {
                secondFileTitle.text = fileTitles[3];
            }
            fileIsEmpty = GetIfFileIsEmpty(saveFileSelection.GetLastLevelForSaveFile(3));
            if (!fileIsEmpty)
            {
                thirdFileTitle.text = fileTitles[2];
            }
            else
            {
                thirdFileTitle.text = fileTitles[3];
            }
        }

        private void DetermineLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }
        
        // Author : Vivianne Lord
        private void InitializeAllText()
        {
            fileSelectionLabels = languageModel.Labels;
            fileTitles = languageModel.Titles;
            fileSelectionButtons = languageModel.Buttons;
            levelNames = languageModel.Names;
        }
        
        
    }
}