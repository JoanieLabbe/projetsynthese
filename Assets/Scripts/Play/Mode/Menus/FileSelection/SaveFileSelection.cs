﻿using System.IO;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbé et Carol-Ann Collin
    [Findable(Tags.MainController)]
    public class SaveFileSelection : MonoBehaviour
    {
        private string filePath;
        private LevelLoadedEventChannel levelLoadedEventChannel;
        private const int MAX_LEVEL = 5;
        private const int MIN_LEVEL = 1;

        public int SaveFile { get; private set; }

        public LevelSelectionModel Model { get; private set; }

        private void Awake()
        {
            levelLoadedEventChannel = Finder.LevelLoadedEventChannel;
            levelLoadedEventChannel.OnLevelLoaded += OnLevelLoaded;
        }

        private void OnDestroy()
        {
            levelLoadedEventChannel.OnLevelLoaded -= OnLevelLoaded;
        }

        private void OnLevelLoaded(int level)
        {
            if (level > LastLevelUnlocked)
            {
                LastLevelUnlocked = level;
                SaveFileValue();
            }
        }

        public void ChooseSaveFile(int saveFile)
        {
            filePath = GetFilePath(saveFile);
            SaveFile = saveFile;

            if (!File.Exists(filePath))
            {
                CreateSaveFile();
            }
            else
            {
                Model = LoadFileValue();
            }
        }

        public void DeleteSaveFile(int saveFile)
        {
            filePath = GetFilePath(saveFile);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        private void CreateSaveFile()
        {
            Model = new LevelSelectionModel {lastLevelUnlocked = 1};
            SaveFileValue();
        }

        public void SaveFileValue()
        {
            File.WriteAllText(filePath, JsonUtility.ToJson(Model));
        }

        public LevelSelectionModel LoadFileValue()
        {
            using (var reader = new StreamReader(filePath))
            {
                return JsonUtility.FromJson<LevelSelectionModel>(reader.ReadToEnd());
            }
        }

        private string GetFilePath(int saveFile)
        {
            return Application.persistentDataPath + "/Save" + saveFile + ".json";
        }

        public int GetLastLevelForSaveFile(int saveFile)
        {
            filePath = GetFilePath(saveFile);
            return File.Exists(filePath) ? LoadFileValue().lastLevelUnlocked : MIN_LEVEL - 1;
        }

        public int LastLevelUnlocked
        {
            get => Model.lastLevelUnlocked;
            private set => Model.lastLevelUnlocked = value <= MAX_LEVEL && value >= MIN_LEVEL ? value : MIN_LEVEL - 1;
        }

        public void AddAchievementIntoList(AchievementEntry entry)
        {
            Model.unlockedAchievements.Add(entry);
        }

        public void SetCoffeeAsTaken()
        {
            Model.level1CoffeeTaken = true;
        }
    }
}