﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class HomeLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] Labels { get; private set; }
        public string[] Buttons { get; private set; }

        public HomeLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Labels = languageAssets.FrenchAsset.homeLabels;
            Buttons = languageAssets.FrenchAsset.homeButtons;
        }

        public void SetEnglish()
        {
            Labels = languageAssets.EnglishAsset.homeLabels;
            Buttons = languageAssets.EnglishAsset.homeButtons;
        }

        public void SetNorwegian()
        {
            Labels = languageAssets.NorwegianAsset.homeLabels;
            Buttons = languageAssets.NorwegianAsset.homeButtons;
        }
    }
}