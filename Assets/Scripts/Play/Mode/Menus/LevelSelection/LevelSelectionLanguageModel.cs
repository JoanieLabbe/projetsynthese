﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class LevelSelectionLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] Labels { get; private set; }
        public string[] Names { get; private set; }
        public string[] Status { get; private set; }
        public string[] Buttons { get; private set; }

        public LevelSelectionLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Labels = languageAssets.FrenchAsset.levelSelectionLabels;
            Names = languageAssets.FrenchAsset.levelNames;
            Status = languageAssets.FrenchAsset.levelStatus;
            Buttons = languageAssets.FrenchAsset.levelSelectionButtons;
        }

        public void SetEnglish()
        {
            Labels = languageAssets.EnglishAsset.levelSelectionLabels;
            Names = languageAssets.EnglishAsset.levelNames;
            Status = languageAssets.EnglishAsset.levelStatus;
            Buttons = languageAssets.EnglishAsset.levelSelectionButtons;
        }

        public void SetNorwegian()
        {
            Labels = languageAssets.NorwegianAsset.levelSelectionLabels;
            Names = languageAssets.NorwegianAsset.levelNames;
            Status = languageAssets.NorwegianAsset.levelStatus;
            Buttons = languageAssets.NorwegianAsset.levelSelectionButtons;
        }
    }
}