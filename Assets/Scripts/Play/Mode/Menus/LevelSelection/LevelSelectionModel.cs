﻿using System;
using System.Collections.Generic;

namespace Game
{
    // Author : Joanie Labbé
    [Serializable]
    public class LevelSelectionModel
    {
        public int lastLevelUnlocked;
        public List<AchievementEntry> unlockedAchievements = new List<AchievementEntry>();
        public bool level1CoffeeTaken;
    }
}