﻿using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault 
    public class OptionsMenu : MonoBehaviour, IMenu
    {
        [Header("Sprite")]
        [SerializeField] private Sprite selectedButtonSprite;
        [SerializeField] private Sprite unselectedButtonSprite;
        
        private const string FORMAT = "{0}%";
        private const string ENIVRONMENT_PARAMETER = "environmentVolume";
        private const string STEP_PARAMETER = "stepsVolume";
        private const int MULTIPLICATOR = 100;
        private const int LOG_CHANGER = 20;

        private Main main;
        private FlagUpdates flag;
        private InputActions.MenuActions menuInputs;
        private MenuNavigator menuNavigator;
        
        private OptionsController controller;
        [SerializeField] private AudioMixer audioMixer;
        private LanguageAssets languageAssets;

        private Canvas optionsCanvas;

        private Button backButton;
        private Button activatedButton;
        private Button deactivatedButton;
        private Button frenchButton;
        private Button englishButton;
        private Button norwegianButton;
        private TextMeshProUGUI[] backButtonLabel;
        private TextMeshProUGUI[] activatedButtonLabel;
        private TextMeshProUGUI[] deactivatedButtonLabel;
        private TextMeshProUGUI[] frenchButtonLabel;
        private TextMeshProUGUI[] englishButtonLabel;
        private TextMeshProUGUI[] norwegianButtonLabel;
        private Image activatedButtonImage;
        private Image deactivatedButtonImage;
        
        private TextMeshProUGUI stepLabel;
        private TextMeshProUGUI environmentLabel;
        private Slider stepSlider;
        private Slider environmentSlider;

        private TextMeshProUGUI stepPercentage;
        private TextMeshProUGUI environmentPercentage;
        private float stepValue;
        private float environmentValue;

        private TextMeshProUGUI title;
        private TextMeshProUGUI fullscreenLabel;
        private TextMeshProUGUI languageLabel;
        
        private void Awake()
        {
            main = Finder.Main;
            flag = Finder.FlagUpdates;
            
            menuInputs = Finder.Inputs.Actions.Menu;
            menuNavigator = Finder.MenuNavigator;
            controller = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
            optionsCanvas = GetComponentInChildren<Canvas>();

            InitializeButtons();
            InitializeSliders();
            InitializeLabels();

            controller.InitializeController(this);
            controller.GetInitialValues();
            stepValue = controller.Values.stepsVolumeValue;
            environmentValue = controller.Values.environmentPercentageValue;
        }
        
        private void Start()
        {
            menuInputs.Enable();
            backButton.Select();
            menuNavigator.Push(this);
        }
        
        private void OnEnable()
        {
            backButton.onClick.AddListener(ReturnToPreviousMenu);
            activatedButton.onClick.AddListener(ActivateFullscreen);
            deactivatedButton.onClick.AddListener(DeactivateFullscreen);
            frenchButton.onClick.AddListener(SetFrench);
            englishButton.onClick.AddListener(SetEnglish);
            norwegianButton.onClick.AddListener(SetNorwegian);

            stepSlider.onValueChanged.AddListener(ChangePercentageStepSounds);
            environmentSlider.onValueChanged.AddListener(ChangePercentageEnvSounds);
        }

        private void OnDisable()
        {
            backButton.onClick.RemoveListener(ReturnToPreviousMenu);
            activatedButton.onClick.RemoveListener(ActivateFullscreen);
            deactivatedButton.onClick.RemoveListener(DeactivateFullscreen);
            frenchButton.onClick.RemoveListener(SetFrench);
            englishButton.onClick.RemoveListener(SetEnglish);
            norwegianButton.onClick.RemoveListener(SetNorwegian);
            
            stepSlider.onValueChanged.RemoveListener(ChangePercentageStepSounds);
            environmentSlider.onValueChanged.RemoveListener(ChangePercentageEnvSounds);
        }
        
        private void OnDestroy()
        {
            menuNavigator.Pop();
        }

        private void Update()
        {
            SelectButtonIfNoneSelected();
        }

        private void InitializeButtons()
        {
            var buttons = GetComponentsInChildren<Button>();
            
            backButton = buttons.WithName(GameObjects.Back);
            activatedButton = buttons.WithName(GameObjects.Activated);
            deactivatedButton = buttons.WithName(GameObjects.Desactivated);
            frenchButton = buttons.WithName(GameObjects.French);
            englishButton = buttons.WithName(GameObjects.English);
            norwegianButton = buttons.WithName(GameObjects.Norwegian);

            backButtonLabel = backButton.GetComponentsInChildren<TextMeshProUGUI>();
            activatedButtonLabel = activatedButton.GetComponentsInChildren<TextMeshProUGUI>();
            deactivatedButtonLabel = deactivatedButton.GetComponentsInChildren<TextMeshProUGUI>();
            frenchButtonLabel = frenchButton.GetComponentsInChildren<TextMeshProUGUI>();
            englishButtonLabel = englishButton.GetComponentsInChildren<TextMeshProUGUI>();
            norwegianButtonLabel = norwegianButton.GetComponentsInChildren<TextMeshProUGUI>();

            activatedButtonImage = activatedButton.GetComponent<Image>();
            deactivatedButtonImage = deactivatedButton.GetComponent<Image>();
        }

        private void InitializeSliders()
        {
            var sliders = GetComponentsInChildren<Slider>();
            
            stepSlider = sliders.WithName(GameObjects.SliderSteps);
            environmentSlider = sliders.WithName(GameObjects.SliderEnvironment);
        }

        private void InitializeLabels()
        {
            var labels = GetComponentsInChildren<TextMeshProUGUI>();

            title = labels.WithName(GameObjects.Title);
            
            stepPercentage = labels.WithName(GameObjects.StepPercentage);
            environmentPercentage = labels.WithName(GameObjects.EnvPercentage);
            
            stepLabel = labels.WithName(GameObjects.LblStepsSound);
            environmentLabel = labels.WithName(GameObjects.LblEnvironmentSounds);

            fullscreenLabel = labels.WithName(GameObjects.LblFullscreen);

            languageLabel = labels.WithName(GameObjects.LblLanguage);
        }

        public void SetInitialValues(OptionsValues optionsValues)
        {
            var stepVolumePercentage = optionsValues.stepsVolumeValue * MULTIPLICATOR;
            stepPercentage.text = string.Format(FORMAT, Mathf.Round(stepVolumePercentage));
            
            var environmentVolumePercentage = optionsValues.environmentPercentageValue * MULTIPLICATOR;
            environmentPercentage.text = string.Format(FORMAT, Mathf.Round(environmentVolumePercentage));
            
            stepSlider.value = optionsValues.stepsVolumeValue;
            environmentSlider.value = optionsValues.environmentPercentageValue;

            if (!optionsValues.fullScreenActivated)
            {
                activatedButtonImage.sprite = unselectedButtonSprite;
                deactivatedButtonImage.sprite = selectedButtonSprite;
            }
            else
            {
                activatedButtonImage.sprite = selectedButtonSprite;
                deactivatedButtonImage.sprite = unselectedButtonSprite;
            }
            
            switch (optionsValues.language)
            {
                case Languages.FR:
                    SetFrench();
                    break;
                case Languages.ENG:
                    SetEnglish();
                    break;
                case Languages.NOR:
                    SetNorwegian();
                    break;
            }
        }

        private void ReturnToPreviousMenu()
        {
            flag.PauseCanvasShouldUpdate = true;
            
            controller.SetEnvironmentVolumeValue(environmentValue);
            controller.SetStepVolumeValue(stepValue);
            controller.SaveOptionsValue();
            
            IEnumerator Routine()
            {
                yield return main.UnloadOptionsScenes();
            }
            StartCoroutine(Routine());
        }

        private void ActivateFullscreen()
        {
            activatedButtonImage.sprite = selectedButtonSprite;
            deactivatedButtonImage.sprite = unselectedButtonSprite;
            controller.SetFullScreenActivity(true);
        }

        private void DeactivateFullscreen()
        {
            activatedButtonImage.sprite = unselectedButtonSprite;
            deactivatedButtonImage.sprite = selectedButtonSprite;
            controller.SetFullScreenActivity(false);
        }

        private void ChangePercentageStepSounds(float newValue)
        {
            audioMixer.SetFloat(STEP_PARAMETER , Mathf.Log(newValue)*LOG_CHANGER);
            
            var percentage = Mathf.FloorToInt(newValue * MULTIPLICATOR);
            stepPercentage.text = string.Format(FORMAT, percentage);
            stepValue = newValue;
        }

        private void ChangePercentageEnvSounds(float newValue)
        {
            audioMixer.SetFloat(ENIVRONMENT_PARAMETER, Mathf.Log(newValue)*LOG_CHANGER);
            
            var percentage = Mathf.FloorToInt(newValue * MULTIPLICATOR);
            environmentPercentage.text = string.Format(FORMAT, percentage);
            environmentValue = newValue;
        }

        // Author : Vivianne Lord
        private void SetFrench()
        {
            foreach (var t in backButtonLabel)
            {
                t.text = languageAssets.FrenchAsset.optionsButtons[2];
            }
            foreach (var t in activatedButtonLabel)
            {
                t.text = languageAssets.FrenchAsset.optionsButtons[0];
            }
            foreach (var t in deactivatedButtonLabel)
            {
                t.text = languageAssets.FrenchAsset.optionsButtons[1];
            }
            foreach (var t in frenchButtonLabel)
            {
                t.text = languageAssets.FrenchAsset.optionsButtons[3];
            }
            foreach (var t in englishButtonLabel)
            {
                t.text = languageAssets.FrenchAsset.optionsButtons[4];
            }
            foreach (var t in norwegianButtonLabel)
            {
                t.text = languageAssets.FrenchAsset.optionsButtons[5];
            }
            
            title.text = languageAssets.FrenchAsset.optionsLabels[0];
            stepLabel.text = languageAssets.FrenchAsset.optionsLabels[1];
            environmentLabel.text = languageAssets.FrenchAsset.optionsLabels[2];
            fullscreenLabel.text = languageAssets.FrenchAsset.optionsLabels[3];
            languageLabel.text = languageAssets.FrenchAsset.optionsLabels[4];
            
            controller.SetGameLanguage(Languages.FR);
        }

        // Author : Vivianne Lord
        private void SetEnglish()
        {
            foreach (var t in backButtonLabel)
            {
                t.text = languageAssets.EnglishAsset.optionsButtons[2];
            }
            foreach (var t in activatedButtonLabel)
            {
                t.text = languageAssets.EnglishAsset.optionsButtons[0];
            }
            foreach (var t in deactivatedButtonLabel)
            {
                t.text = languageAssets.EnglishAsset.optionsButtons[1];
            }
            foreach (var t in frenchButtonLabel)
            {
                t.text = languageAssets.EnglishAsset.optionsButtons[3];
            }
            foreach (var t in englishButtonLabel)
            {
                t.text = languageAssets.EnglishAsset.optionsButtons[4];
            }
            foreach (var t in norwegianButtonLabel)
            {
                t.text = languageAssets.EnglishAsset.optionsButtons[5];
            }
            
            title.text = languageAssets.EnglishAsset.optionsLabels[0];
            stepLabel.text = languageAssets.EnglishAsset.optionsLabels[1];
            environmentLabel.text = languageAssets.EnglishAsset.optionsLabels[2];
            fullscreenLabel.text = languageAssets.EnglishAsset.optionsLabels[3];
            languageLabel.text = languageAssets.EnglishAsset.optionsLabels[4];
            
            controller.SetGameLanguage(Languages.ENG);
        }

        // Author : Vivianne Lord
        private void SetNorwegian()
        {
            foreach (var t in backButtonLabel)
            {
                t.text = languageAssets.NorwegianAsset.optionsButtons[2];
            }
            foreach (var t in activatedButtonLabel)
            {
                t.text = languageAssets.NorwegianAsset.optionsButtons[0];
            }
            foreach (var t in deactivatedButtonLabel)
            {
                t.text = languageAssets.NorwegianAsset.optionsButtons[1];
            }
            foreach (var t in frenchButtonLabel)
            {
                t.text = languageAssets.NorwegianAsset.optionsButtons[3];
            }
            foreach (var t in englishButtonLabel)
            {
                t.text = languageAssets.NorwegianAsset.optionsButtons[4];
            }
            foreach (var t in norwegianButtonLabel)
            {
                t.text = languageAssets.NorwegianAsset.optionsButtons[5];
            }
            
            title.text = languageAssets.NorwegianAsset.optionsLabels[0];
            stepLabel.text = languageAssets.NorwegianAsset.optionsLabels[1];
            environmentLabel.text = languageAssets.NorwegianAsset.optionsLabels[2];
            fullscreenLabel.text = languageAssets.NorwegianAsset.optionsLabels[3];
            languageLabel.text = languageAssets.NorwegianAsset.optionsLabels[4];
            
            controller.SetGameLanguage(Languages.NOR);
        }
        
        private void SelectButtonIfNoneSelected()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                backButton.Select();
            }
        }

        public void Show()
        {
            optionsCanvas.enabled = true;
            backButton.Select();
        }

        public void Hide()
        {
            optionsCanvas.enabled = false;
        }
        
    }
}