﻿using Harmony;

namespace Game
{
    // Author : Vivianne Lord
    public class PauseLanguageModel
    {
        private OptionsController optionsController;
        private LanguageAssets languageAssets;

        public Languages ActiveLanguage => optionsController.Values.language;
        
        public string[] Labels { get; private set; }
        public string[] Buttons { get; private set; }

        public PauseLanguageModel()
        {
            optionsController = Finder.OptionsController;
            languageAssets = Finder.LanguageAssets;
        }
        
        public void SetFrench()
        {
            Labels = languageAssets.FrenchAsset.pauseLabels;
            Buttons = languageAssets.FrenchAsset.pauseButtons;
        }

        public void SetEnglish()
        {
            Labels = languageAssets.EnglishAsset.pauseLabels;
            Buttons = languageAssets.EnglishAsset.pauseButtons;
        }

        public void SetNorwegian()
        {
            Labels = languageAssets.NorwegianAsset.pauseLabels;
            Buttons = languageAssets.NorwegianAsset.pauseButtons;
        }
    }
}