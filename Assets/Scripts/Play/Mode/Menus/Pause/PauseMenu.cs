﻿using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    // Author : Kamylle Thériault
    public class PauseMenu : MonoBehaviour, IMenu
    {
        private Main main;
        private FlagUpdates flag;
        private InputActions.MenuActions menuInputs;
        private MenuNavigator menuNavigator;
        private PauseLanguageModel languageModel;
        private LanguageEventChannel languageEventChannel;
        
        private Button resumeButton;
        private Button optionsButton;
        private Button returnButton;
        private Button[] buttons;
        
        private TextMeshProUGUI[] resumeButtonText;
        private TextMeshProUGUI[] optionsButtonText;
        private TextMeshProUGUI[] returnButtonText;
        private TextMeshProUGUI pauseTitle;

        private Canvas pauseCanvas;

        private bool IsPaused => Time.timeScale == 0;
        
        private void Awake()
        {
            main = Finder.Main;
            flag = Finder.FlagUpdates;
            
            menuInputs = Finder.Inputs.Actions.Menu;
            menuNavigator = Finder.MenuNavigator;
            pauseCanvas = GetComponentInChildren<Canvas>();
            languageModel = new PauseLanguageModel();
            languageEventChannel = Finder.LanguageEventChannel;
            languageEventChannel.OnLanguageChangeEvent += ChangeLanguage;
            
            SetActiveLanguage();
            pauseTitle = GetComponentsInChildren<TextMeshProUGUI>().WithName(GameObjects.Title);
            pauseTitle.text = languageModel.Labels[0];
            FindButtonsInView();
            UpdateCanvas(IsPaused);
        }

        private void Start()
        {
            menuInputs.Enable();
            resumeButton.Select();
            menuNavigator.Push(this);
            pauseCanvas.enabled = IsPaused;
        }
        
        private void OnEnable()
        {
            resumeButton.onClick.AddListener(ResumeGame);
            optionsButton.onClick.AddListener(OpenOptionsMenu);
            returnButton.onClick.AddListener(ReturnToMainMenu);
        }

        private void OnDisable()
        {
            resumeButton.onClick.RemoveListener(ResumeGame);
            optionsButton.onClick.RemoveListener(OpenOptionsMenu);
            returnButton.onClick.RemoveListener(ReturnToMainMenu);
        }
        
        private void OnDestroy()
        {
            menuNavigator.InverseMenuOrder();
            languageEventChannel.OnLanguageChangeEvent -= ChangeLanguage;
        }
        
        // Author : Marc-Antoine Sigouin
        private void Update()
        {
            if (pauseCanvas.enabled)
            {
                SelectButtonIfNoneSelected();
            }
            if (menuInputs.Back.triggered || (!IsPaused && menuInputs.enabled))
            {
                Time.timeScale = 1f;
                
                UpdateCanvas(IsPaused);
                
                menuInputs.Disable();
            }
            if (IsPaused && !pauseCanvas.enabled && flag.PauseCanvasShouldUpdate)
            {
                UpdateCanvas(IsPaused);
                menuInputs.Enable();
            }
        }

        private void FindButtonsInView()
        {
            buttons = GetComponentsInChildren<Button>();
            resumeButton = buttons.WithName(GameObjects.Resume);
            optionsButton = buttons.WithName(GameObjects.Options);
            returnButton = buttons.WithName(GameObjects.Return);

            SetLabelsToButtons();
        }

        private void SetLabelsToButtons()
        {
            resumeButtonText = resumeButton.GetComponentsInChildren<TextMeshProUGUI>();
            optionsButtonText = optionsButton.GetComponentsInChildren<TextMeshProUGUI>();
            returnButtonText = returnButton.GetComponentsInChildren<TextMeshProUGUI>();

            for (int g = 0; g < resumeButtonText.Length; g++)
            {
                resumeButtonText[g].text = languageModel.Buttons[0];
                optionsButtonText[g].text = languageModel.Buttons[1];
                returnButtonText[g].text = languageModel.Buttons[2];
            }
        }

        // Author : Vivianne Lord
        private void SetActiveLanguage()
        {
            switch (languageModel.ActiveLanguage)
            {
                case Languages.FR:
                    languageModel.SetFrench();
                    break;
                case Languages.ENG:
                    languageModel.SetEnglish();
                    break;
                case Languages.NOR:
                    languageModel.SetNorwegian();
                    break;
            }
        }

        // Author : Marc-Antoine Sigouin
        private void UpdateCanvas(bool isActive)
        {
            SetButtonsActivity(isActive);
            pauseCanvas.enabled = isActive;
            if (isActive)
            {
                resumeButton.Select();
            }
        }

        private void ResumeGame()
        {
            Time.timeScale = 1f;
            UpdateCanvas(IsPaused);
        }

        private void OpenOptionsMenu()
        {
            IEnumerator Routine()
            {
                flag.PauseCanvasShouldUpdate = false;
                yield return main.LoadOptionsScenes();
            }
            
            StartCoroutine(Routine());
        }
        
        private void ReturnToMainMenu()
        {
            IEnumerator Routine()
            {
                yield return main.LoadHomeScenes();
                yield return main.UnloadGameScenes();
            }
            
            StartCoroutine(Routine());
        }
        
        // Author : Kamylle Thériault
        public void SelectButtonIfNoneSelected()
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                resumeButton.Select();
            }
        }
        
        public void Show()
        {
            UpdateCanvas(true);
            resumeButton.Select();
        }

        public void Hide()
        {
            UpdateCanvas(false);
        }
        
        // Author : Joanie Labbe
        private void SetButtonsActivity(bool shouldBeEnabled)
        {
            foreach (var button in buttons)
            {
                button.enabled = shouldBeEnabled;
            }
        }

        private void ChangeLanguage()
        {
            SetActiveLanguage();
            
            pauseTitle.text = languageModel.Labels[0];
            for (int g = 0; g < resumeButtonText.Length; g++)
            {
                resumeButtonText[g].text = languageModel.Buttons[0];
                optionsButtonText[g].text = languageModel.Buttons[1];
                returnButtonText[g].text = languageModel.Buttons[2];
            }
        }
    }
}