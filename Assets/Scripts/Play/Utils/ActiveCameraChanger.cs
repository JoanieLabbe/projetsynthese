﻿using Cinemachine;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Vivianne Lord
    [Findable(Tags.GameController)]
    public class ActiveCameraChanger : MonoBehaviour
    {
        private CinemachineVirtualCamera currentVCam;

        public void DeactivateCurrentCamera(Transform toFollow)
        {
            currentVCam = GameObject.FindWithTag(Tags.StartCamera).GetComponent<CinemachineVirtualCamera>();
            currentVCam.Follow = toFollow;
        }
    }
}