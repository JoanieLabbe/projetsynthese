﻿using DG.Tweening;

namespace Game
{
    // Author : Joanie Labbe
    public static class LightSourceExtension
    {
        public static void DotweenTo(this LightingSource2D lightingSource, float sizeWanted, float delay)
        {
            DOTween.To(
                () => lightingSource.lightSize,
                value => lightingSource.lightSize = value,
                sizeWanted,
                delay
            );
        }
    }
}