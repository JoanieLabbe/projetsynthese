﻿using DG.Tweening;
using UnityEngine;

namespace Game
{
    // Author : Joanie Labbe
    public static class LightSpriteRendererExtension
    {
        public static void DotweenTo(this LightingSpriteRenderer2D lightSpriteRenderer, Vector2 sizeWanted, float delay)
        {
            DOTween.To(
                () => lightSpriteRenderer.offsetScale,
                value => lightSpriteRenderer.offsetScale = value,
                sizeWanted,
                delay
            );
        }
    }
}