﻿using System.Collections.Generic;

namespace Game
{
    // Author : Joanie Labbé
    public static class ListExtension
    {
        public static void Copy(this List<Waypoint> elements, List<Waypoint> elementToCopy)
        {
            elements.Clear();
            foreach (var element in elementToCopy)
            {
                elements.Add(element);
            }
        }
    }
}