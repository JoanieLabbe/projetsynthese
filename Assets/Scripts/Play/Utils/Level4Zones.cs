﻿namespace Game
{
    // Author : Kamylle Thériault
    public enum Level4Zones
    {
        PedestrianCrossing,
        Construction,
        TopRoad,
        SmallAlley
    }
}