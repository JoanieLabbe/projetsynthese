﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author : Carol-Ann Collin
    [Findable(Tags.SoundPlayer)]
    public class SoundPlayer : MonoBehaviour
    {
        [SerializeField][Range(1, 5)] private int audioSourceCount;
        
        private readonly List<AudioSource> audioSources = new List<AudioSource>();

        private void Awake()
        {
            for (var i = 0; i < audioSourceCount; i++)
            {
                audioSources.Add(gameObject.AddComponent<AudioSource>());
            }
        }

        public void PlaySound(AudioClip clip, float volume = 0f, bool loop = false)
        {
            foreach (var source in audioSources)
            {
                if (!source.isPlaying)
                {
                    source.clip = clip;
                    if (volume > 0)
                    {
                        source.volume = volume;
                    }
                    source.loop = loop;
                    source.Play();
                    break;
                }
            }
        }

        public void StopSound(AudioClip clip)
        {
            foreach (var source in audioSources)
            {
                if (source.isPlaying && source.clip == clip)
                {
                    source.Stop();
                    break;
                }
            }
        }
    }
}