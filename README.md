# Blurry Days

Blurry days est un jeu 2D créé dans le cadre du cours "Projet de développement de jeux vidéo 420-V51-SF". Le jeu au travers d'une caméra en position _top down view_ relate une journée typique de Jean-Simon, jeune adulte atteint de cécité, et de son chien-guide Pantoufle. C'est dans un monde complètement noir sauf pour tout ce qui émet un son qu'est plongé le joueur pour comprendre les défis que les personnes atteintes de cet handicap doivent relever, tout en dépassant leurs capacités.

## Démarrage rapide

Ces instructions vous permettront d'obtenir une copie opérationnelle du projet sur votre machine à des fins de développement.

### Prérequis

* [Git](https://git-scm.com/downloads) - Système de contrôle de version. Utilisez la dernière version.
* [Rider](https://www.jetbrains.com/rider/) ou [Visual Studio](https://www.visualstudio.com/fr/) - IDE. Vous pouvez utiliser 
  également n'importe quel autre IDE: assurez-vous simplement qu'il supporte les projets Unity.
* [Unity 2020.1.7f1](https://unity3d.com/fr/get-unity/download/) - Moteur de jeu. Veuillez utiliser **spécifiquement cette 
  version.** Attention à ne pas installer Visual Studio une seconde fois si vous avez déjà un IDE.

**Attention!** Actuellement, seul le développement sur Windows est complètement supporté.

### Compiler une version de développement

Clonez le projet.

```
git clone https://gitlab.com/JoanieLabbe/projetsynthese.git
```

Ouvrez le projet dans Unity. Ensuite, ouvrez la scène `Main` et appuyez sur le bouton *Play*.

### Tester un version stable ou de développement

Ouvrez le projet dans Unity. Ensuite, allez dans `File > Build Settings…` et compilez le projet **dans un dossier vide**.

Si vous rencontrez un bogue, vous êtes priés de le [signaler](https://gitlab.com/JoanieLabbe/projetsynthese/issues/new?issuable_template=Bug).
Veuillez fournir une explication détaillée de votre problème avec les étapes pour reproduire le bogue. Les captures d'écran et 
les vidéos jointes sont les bienvenues.

## Contribuer au projet

Veuillez lire [CONTRIBUTING.md](CONTRIBUTING.md) pour plus de détails sur notre code de conduite.

## Auteurs

* **Carol-Ann Collin** - *Programmeuse*
  * Programmation UI du HUD et implémentations des accomplissements. Création du joueur.
  * Création du niveau 3.
* **Joanie Labbé** - *Programmeuse*
  * Programmation des utilitaires, des passage piétons et du sonar. 
  * Implémentation de la sauvegarde de la progression.
  * Création du niveau 2.
* **Vivianne Lord** - *Programmeuse*
  * Programmation du tutoriel et des déplacements des ennemis.
  * Création du niveau 1.
* **Marc-Antoine Sigouin** - *Programmeur*
  * Programmation de la complétion de niveau.
  * Création du niveau 5.
* **Kamylle Thériault** - *Programmeuse et Artiste 2D*
  * Programmation UI des menus et des dialogues.
  * Artiste des cinématiques et des sprites du jeu.
  * Création du niveau 4.

## Remerciements
* Benjamin Lemelin - Extensions sur le moteur Unity pour la recherche d'objets et de composants. Générateur de constantes. Gestionnaire de chargement des scènes.
* François Paradis - Pour les tests et pour les retours sur les différents bugs
* Yannick Mazières - L'expertise sur la gestion de projet et les techniques de développement agile
* Mathieu Harharidis - Son expérience et ses conseils précieux
* [ZapSplat](https://www.zapsplat.com/) et [Freesound](https://freesound.org/) - Pour la majorité des sons d'ambiance et des effets sonores
* Keenfilms - Pour l'[ambiance sonore du bureau](https://www.youtube.com/watch?v=xPVDqUqQYYI)
* FunkyCode - Pour [son Asset Smart Lighting 2D](https://assetstore.unity.com/packages/tools/particles-effects/smart-lighting-2d-112535) dans Unity
* Michsky - Pour [son Asset Dark UI](https://assetstore.unity.com/packages/tools/gui/dark-complete-grunge-ui-149914) dans Unity
